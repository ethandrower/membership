from django.apps import AppConfig


class DiscordAppConfig(AppConfig):
    name = 'discord_app'

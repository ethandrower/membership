from django.shortcuts import render
import api.models as models
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

# Create your views here.

@csrf_exempt
def check_discord_alerts(request):

    if request.method =='POST':
        print("inside post")

        trade_id = request.POST['trade_id']
        status = request.POST['status']

        

        alert_trigger = models.NotifyTrigger.objects.get(trade__id=trade_id)
        alert_trigger.discord_status = status
        alert_trigger.save()
    else:

        alert_triggers = models.NotifyTrigger.objects.filter(discord_status=None)
        print("got alert triggers: "  + str(alert_triggers))
        for trigger in alert_triggers:
            print("trigger" + str(trigger))
            print(str(trigger.trade))
            

            alert_string = """TRADE DATE - {0} \n \n {1} \n {2} \n {3} \n TRADE COMMENTS \n {4} 
                """.format(trigger.trade.trade_time.strftime('%d-%b-%Y'), trigger.trade.trade_type.upper(),  trigger.trade.tos_string, trigger.trade.tos_string2, trigger.trade.comments)

            trigger.discord_status = True
            trigger.save()

            return JsonResponse({"alert_text": alert_string})

        return JsonResponse({"alert_text": False})


@csrf_exempt
def update_discord_username(request):

    if request.method == 'POST':

        email = request.POST['email']
        username = request.POST['username']

        try:

            sub = models.Subscriber.objects.get(email=email)
            sub.discord_id = username
            sub.save()
            return JsonResponse(status=200, data={'status': "success"})
        except Exception as e:
            print("exception in update discord: " + str(e))
            return JsonResponse(status=500, data={'status': "error", "msg": "could not update subscriber" })
    else:

        discord_id = request.GET['discord_id']

        try:
            sub = models.Subscriber.objects.get(discord_id=discord_id)

            return JsonResponse(status=200, data={"status": "success", "is_daytrader": sub.is_daytrader, "annual": sub.annual, "trade_alerts": sub.trade_alerts })

        except Exception as e:
            print("exception in update discord: " + str(e))
            return JsonResponse(status=500, data={'status': "error", "msg": "could not find that discord id" })

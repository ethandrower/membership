"""navigation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin


from api.views import *
from api.myviews.cancelflow import *
from api.myviews.archive import * 
from api.myviews.discourseauth import * 
from discord_app.views import check_discord_alerts, update_discord_username
from django.conf import settings # new
from django.urls import path, include # new
from django.conf.urls.static import static # new


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('navigation/', receive_token),
    path('delivery/', delivery_report),
    path('home/', trade_view_home),
    path('', trade_view_home),
    path('tradedetail/', trade_update),
    path('new/', new_futures_trade),
    path('new-ghost/', new_ghost_trade),
    path('thinkific-trade-alerts/', get_alerts_thinkific), 
    path('thinkific-positions/', get_positions),
    path('newuser/', new_user),  #zapier webhook
    path('checkmember/', check_membership),
    path('profit/', update_profit),
    path('update-leg/', handle_leg_update),

    path('expiration/', handle_new_expiration_trade),
    path('discount/', discount_request),
    path('discount-auto/', price_plan_discount),
    path('archives/', archive_page),
    path('archives-closed/', archive_page_closed),
    path('sso/', handle_discourse_1),
    path('sso2/', handle_discourse_2),
    path('newcust/', handle_new_subscriber),
    path('cancel/', cancel_request),
    path('live/', check_uptime),
    path('discord/', update_discord_username),
    path('discord-trades/', check_discord_alerts),

    
   # path('', include('trade.urls')), # new
    

]



if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

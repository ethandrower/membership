
import discord
import asyncio
from discord import Intents, Invite
import requests
import os

class MyClient(discord.Client):

    # guild for ethandrower server 751896804106174624
    guild_id = 712582337744994376  # nav trading. 
    trading_role_id = 712600423852146710  # nav trading
    # trading role ethandrower server 751898525834412063
    pro_member_role = 712600423852146710 # nav 
    dt_member_role =  774390267130019860


    async def on_ready(self):
        

        await self.timer()

            #asyncio.run()

    async def update_email(self, user_obj):

        api_url = os.getenv('DJANGO_URL')

        res = requests.post(api_url + 'discord/', data=user_obj)

        if res.status_code == 200:
            return True

        else:
            return False
    async def post_trade(self, alert_text):

        channel = self.get_channel(770372349643194409)
        await channel.send(alert_text)


    async def check_for_trades(self):
        api_url = os.getenv('DJANGO_URL')

        res = requests.get(api_url + 'discord-trades/')

        obj = res.json()
        if obj['alert_text']:

            await self.post_trade(obj['alert_text'])


    #async def update_trades(self, trade_id):


    async def check_user(self, discord_id):

        api_url = os.getenv('DJANGO_URL')

        res = requests.get(api_url + 'discord/', params={'discord_id': discord_id})


        user = res.json()
        print("Response from API for User: {0} {1}".format(discord_id, user))
        if user['status'] == 'success':
            return user
            return user['is_daytrader']

        else:
            return False


    async def update_single_user(self, member):
        user = await self.check_user(member.name)
        guild = self.get_guild(self.guild_id)
#       member = guild.get_member(user.id)
        #trading_role = guild.get_role(self.trading_role_id) # pro members
        dt_role = guild.get_role(self.dt_member_role) 
        pro_role = guild.get_role(self.pro_member_role)

        try:

            if user:

                if user['is_daytrader']:
                    print("adding dt role for {0}".format(member.name))
                    await member.add_roles(dt_role)
                    #await member.add_roles(pro_role)
                elif user['annual'] and user['trade_alerts']:

                    print("adding dt role for annual member {0}".format(member.name))
                    await member.add_roles(dt_role)
                else:
                    print("removing DT role for {0}".format(member.name))
                    await member.remove_roles(dt_role)



                if user['trade_alerts']:

                    print("Adding Pro role for {0}".format(member.name))
                    await member.add_roles(pro_role)

                else:

                    print("removing Pro role for {0}".format(member.name))
                    await member.remove_roles(pro_role)
        except Exception as e:
                    print("error adding or removingroles with user " + str(e))

        # if is_daytrader:
        # #result = loop.run_until_complete(member.add_roles(trading_role))
        #   print("daytrader found, adding roles")
        #   await member.add_roles(trading_role)

        # else:
        #   await member.remove_roles(trading_role)


    @asyncio.coroutine
    async def timer(self):

        while True:


            await asyncio.sleep(506)
            await self.check_for_trades()
            print("looping")
            print("logged in as {0}".format(self.user))
            users = self.users
            print(str(users))

            print(self.user)
            
            guilds = self.guilds        
            print("guilds: " + str(guilds))

            # guild id = 751896804106174624 #ethandrower server
            guild = self.get_guild(self.guild_id) # nav daytring options for income
            print("guild gotten: " + str(guild))
            print("guild members: " + str(guild.members))

            print("guild roles: " + str(guild.roles))

            ## guild.get_role(id)  gets our trading roles
            #trading_role = guild.get_role(751898525834412063) # pro members ethandrower server

            trading_role = guild.get_role(self.trading_role_id) # nav daytrading options server

            print("trading role: " + str(trading_role))
            #everyone_role = guild.get_role(751896804106174624) #everyone
            #loop = asyncio.get_event_loop()
            print("Guild Members: " + str(guild.members))

            for member in guild.members:
                #if str(member) != "ethandrower_citemed#0627":

                print("adding roles for member: " + str(member.name))
                await asyncio.sleep(.5)

                status = await self.check_user(member.name)

                if status:
                    print("member to update: "  + str(member))
                    await self.update_single_user(member)
                # if is_daytrader:
                # #result = loop.run_until_complete(member.add_roles(trading_role))
                #   print("daytrader found, adding roles")
                #   await member.add_roles(trading_role)

                # else:
                #   await member.remove_roles(trading_role)


            print("completed!")

    @asyncio.coroutine
    async def on_member_join(self, member):
        print("member joined {0}".format(member))
        await member.send('Thanks for joining the Navigation Trading Discord Community! Please register your email with me.')
        await member.send('To register, send the command    \'!register\'    followed by your Nav trading email address')
        await member.send('Like this...')
        await member.send('!register myemail@gmail.com')

    @asyncio.coroutine
    async def on_message(self, message):
        print("message recieved: {0}".format(message))

        username = message.author
        print("username: " + str(username.name))
        mentions = message.mentions
        to_bot = True if str(message.channel).find("Direct Message") != -1 else False
        #print(str(message.recipient))
        #print(str(message.mentions))
        #print("message channel: " + str(message.channel))
        #print("message channel recipient: "  + str(message.channel.me.name))
        

        if username.name != 'NAV_BOT' and to_bot:

            if message.content[0] == '!':
                #we have a command
                if message.content.split(' ')[0].lower() == '!register':
                    #register email
                    email = message.content.split(' ')[1].lower()
                    print("email: " + email)

                    data = {"email": email.strip(), "username": username.name.strip() }

                    update_id = await self.update_email(data)

                    if update_id:
                        await message.channel.send('User Id is Updated! Your access will be granted shortly.')
                        await self.update_single_user(username)
                    else:
                        await message.channel.send('You are currently a FREE member, if you would like to upgrade to our premium membership, please use this link to join now https://navigationtrading.com/product')
                        await message.channel.send('If you are already registered as a Pro member, contact support to fix your access.')





                    # make django call here.


            else:

                await message.channel.send('Hi there! I am the Nav Trading bot!  All I can do right now is register Daytrading and Pro subscribers')
                await message.channel.send('To register, send the command    \'!register\'    followed by your Nav trading email address')
                await message.channel.send('Like this...')

                await message.channel.send('!register myemail@gmail.com')





intent = Intents().all() 
print("intent: " + str(intent))
client = MyClient(intents=intent )

client.run('NzUxODg3NTc4NTI0OTQyMzY2.X1Pn4A.gJZSj3RFV3Ojg8EE3KjDidKtco8')




## could also try guild.members  once we get a guild object...


## get all users on the guild


## for each user, hit api and get their django info



### Member.edit function for updating their roles  parameter is 'roles' 
#roles (Optional[List[Role]]) – The member’s new list of roles. This replaces the roles.

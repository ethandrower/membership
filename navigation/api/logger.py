


from api.models import Log, Trade, Subscriber


def write_log(status=None, action=None, trade=None, subscriber=None, comment=None):

    if subscriber:
        try:

            subscriber = Subscriber.objects.get(email=subscriber)
        except Exception as e:
            print("couldn't get subscriber")
            subscriber=None

    if trade:
        trade = Trade.objects.get(id=trade)

    log = Log(status=status, action=action, trade=trade, subscriber=subscriber, comment=comment)
    log.save()


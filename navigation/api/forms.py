

#forms 
from django import forms
from api.models import *

from datetime import datetime
from pytz import timezone

trade_types = (
        ('Opening Trade', 'OPENING TRADE'),
        ('Closing Trade', 'CLOSING TRADE'),
        ('Rolling/Adjusting Trade', 'ROLLING/ADJUSTING TRADE'),
        ('Closing/Adjusting Trade', 'CLOSING/ADJUSTING TRADE'),
        ('Opening/Adjusting Trade', 'OPENING/ADJUSTING TRADE'),
        ('Expiration Trade', 'EXPIRATION TRADE')


    )

status_types = (
    ('OPEN', 'OPEN'),
    ('CLOSED', 'CLOSED')

    )

def get_possible_expiration_trades():


    tup_list = []
    trades = Trade.objects.all().order_by('-trade_time')

    for trade in trades:



        converted_time = trade.trade_time.astimezone(timezone('US/Central'))
        converted_time =  converted_time.strftime("%m/%d/%Y, %H:%M:%S")
        trade_str ="ID: " + str(trade.id )+ "    Time: " + converted_time +   "    " +   str(trade.initial_strategy) + "    Symbol: " + str(trade.underlying) 
        tup_list.append( (trade_str, trade_str) )

    return tuple(tup_list)



class ExpirationTradeForm(forms.Form):
    

    def __init__(self, *args, **kwargs):
        super(ExpirationTradeForm, self).__init__(*args, **kwargs)
        self.fields['exp_trade'] = forms.ChoiceField( choices=get_possible_expiration_trades() )



    trade_id = forms.CharField(required=False, label='Exact Trade ID (Not required if you use the drop-down')

    exp_trade = forms.CharField(required=False)

    # trade_types = (
    #   ('Opening Trade', 'OPENING TRADE'),
    #   ('Closing Trade', 'CLOSING TRADE'),
    #   ('Rolling/Adjusting Trade', 'ROLLING/ADJUSTING TRADE'),
    #   ('Closing/Adjusting Trade', 'CLOSING/ADJUSTING TRADE'),
    #   ('Opening Adjusting Trade', 'OPENING/ADJUSTING TRADE'),

    # )

### trade status  OPEN/CLosed

### mother trade - prepopulate but allow a change manually  by id enter.






def get_possible_mother_trades(underlying):


    if underlying.find("/") != -1:
        underlying = underlying[0:2]


    trades = Trade.objects.filter(trade_type='Opening Trade', status='OPEN', underlying__contains=underlying)\
            | Trade.objects.filter(trade_type='OPENING TRADE', status='OPEN', underlying__contains=underlying)



    tup_list = []
    tup_list.append( ("Root Trade", "Root Trade") )

    for trade in trades:

        print("Original Trade time " + str( trade.trade_time.tzinfo)  + trade.trade_time.strftime("%m/%d/%Y, %H:%M:%S") )

        converted_time = trade.trade_time.astimezone(timezone('US/Central'))
        converted_time =  converted_time.strftime("%m/%d/%Y, %H:%M:%S")

        trade_str ="ID: " + str(trade.id )+ "    Time: " + converted_time +   "    " +   trade.initial_strategy + "    Symbol: " + trade.underlying 
        tup_list.append( (trade_str, trade_str) )


    print(tuple(tup_list))
    return tuple(tup_list)



class TradeUpdateForm(forms.Form):
    

    def __init__(self, *args, **kwargs):

        try:
            underlying = kwargs.pop('test_arg')
            print("testing arg: " + str(underlying))
        except Exception as e:
            print("no underlying to pop")
            underlying = ""

        super(TradeUpdateForm, self).__init__(*args, **kwargs)
        self.fields['mother_trade'] = forms.ChoiceField( choices=get_possible_mother_trades(underlying) )




    # trade_types = (
    #   ('Opening Trade', 'OPENING TRADE'),
    #   ('Closing Trade', 'CLOSING TRADE'),
    #   ('Rolling/Adjusting Trade', 'ROLLING/ADJUSTING TRADE'),
    #   ('Closing/Adjusting Trade', 'CLOSING/ADJUSTING TRADE'),
    #   ('Opening Adjusting Trade', 'OPENING/ADJUSTING TRADE'),

    # )

### trade status  OPEN/CLosed

### mother trade - prepopulate but allow a change manually  by id enter.

    mother_trade = forms.CharField(required=False)
    initial_strategy = forms.CharField()
    trade_type = forms.ChoiceField(choices=trade_types)
    status_after_update = forms.ChoiceField(choices=status_types)
    tos_string = forms.CharField(widget=forms.Textarea, required=False)
    tos_string2 =forms.CharField(widget=forms.Textarea, required=False)
    prev_dte = forms.IntegerField(required=False, label='Previous DTE (if rolling)')
    new_dte = forms.IntegerField(required=False, label='New DTE')
    comments = forms.CharField(widget=forms.Textarea, required=False)
    iv_percentile = forms.DecimalField(required=False)

    screen_shot = forms.FileField(required=False)
    screen_shot2 = forms.FileField(required=False)
    screen_shot3 = forms.FileField(required=False)
    #expiration = forms.DateField(required=False, label='Front Month Exp')
   
    send_alerts = forms.BooleanField(widget=forms.CheckboxInput, required=False )
    trade_id = forms.IntegerField(widget=forms.HiddenInput, required=False)

    initial_strategy = forms.CharField()


class UpdateProfitForm(forms.Form):

    trade_id = forms.CharField(required=True)
    profit = forms.DecimalField(required=False)
    


class NewFuturesTradeForm(forms.Form):

    is_equity = forms.BooleanField(widget=forms.CheckboxInput, required=False )
    is_double_single = forms.BooleanField(widget=forms.CheckboxInput, required=False)
    
    trade_type = forms.ChoiceField(choices=trade_types)
    status_after_update = forms.ChoiceField(choices=status_types)
    trade =forms.CharField(widget=forms.Textarea)
    trade2 =forms.CharField(widget=forms.Textarea, required=False)
    prev_dte = forms.IntegerField(required=False, label='Previous DTE (if rolling)')
    new_dte = forms.IntegerField(required=False, label='New DTE')


    iv_percentile = forms.DecimalField(required=False)

    #expiration = forms.DateField(required=False, label='Front Month Expiration (YYYY-MM-DD)')
    
    
    

class GhostTradeForm(forms.Form):

    trade_type = forms.ChoiceField(choices=trade_types)
    iv_percentile = forms.DecimalField(required=False)
    fill = forms.DecimalField(required=False, label='Fill Price')

    expiration = forms.DateField(required=False, label='Front Month Expiration (YYYY-MM-DD)')
    symbol = forms.CharField(required=False, label='Symbol')
    contract = forms.CharField(required=False, label='Contract')
    strike = forms.DecimalField(required=False, label='Strike') 
    put_call = forms.CharField(required=False, label='PUT/CALL')
    volume = forms.IntegerField(required=False, label='Volume')
    buy_sell = forms.CharField(required=False, label='BUY/SELL')

    initial_strategy = forms.CharField()

    new_dte = forms.IntegerField(required=False, label='New DTE')
    prev_dte = forms.IntegerField(required=False, label='Previous DTE (if rolling)')
    status_after_update = forms.ChoiceField(choices=status_types)
    comments = forms.CharField(widget=forms.Textarea, required=False)




    # trade_type = models.CharField(max_length=250)

    # #legs  we don't need to update them via forms. 

    # #initial_strategy =
    # spread_fill = models.DecimalField( decimal_places=2, max_digits=15 )
    # comments = models.CharField(max_length=2000, null=True, default=None)
    # iv_percentile = models.DecimalField(decimal_places=2, max_digits=15)




from datetime import datetime, timezone, timedelta
import pytz
## utilities for market data and others.
from .models import Trade, TradeLegs 
from .emailer import send_alert


months_dic = {
                1: "January",
                2: "February",
                3: "March",
                4: "April",
                5: "May",
                6: "June",
                7: "July",
                8: "August",
                9: "September",
                10: "October",
                11: "November",
                12: "December"
            }

def process_positions_archives(trades):
    print("starting process_positions for archives" ) 
    trade_objs = []

    ## for each closing trade, 
    for trade in trades:

        if trade.mother_trade is not None:

            mother_trade = Trade.objects.get(id=trade.mother_trade.id)
    # find the root trade 
            print("mo trade found: " + str(mother_trade))


    # find all children of the root trade sorted by tradetime
            children = Trade.objects.all().filter(mother_trade=mother_trade).order_by('trade_time')
            print("len of children: " + str(len(children)))
            children_objs = [] 

            for child in children:
                print("processing child ")
                child_leg_list = []
                #child_legs = TradeLegs.objects.all().filter(trade=child)
                #for child_leg in child_legs:
                #    child_leg_list.append(child_leg)

                children_objs.append({'legs': child_leg_list, 'trade': child })
 
                if child.trade_type.upper() == 'CLOSING TRADE' or child.trade_type.upper() == 'EXPIRATION TRADE':
                    closing_trade_date = child.trade_time


                    if child.profit is not None:
                        trade.profit = child.profit
                        trade.save()

                else:
                    pass


            trade_objs.append({'legs': [], 'exit_date': trade.trade_time, 'trade': mother_trade, 'child_trades': children_objs, 'screen_shots': [] })


    return trade_objs





    for trade in trades:
        leg_list = []
        legs = TradeLegs.objects.all().filter(trade=trade)

        trade.underlying = chop_futures_underlying(trade.underlying)
        trade.underlying = chop_w_underlying(trade.underlying)

        for leg in legs:

            #leg = serializers.serialize('json', [leg, ])

            leg_list.append(leg)

        children = Trade.objects.all().filter(mother_trade=trade).order_by('trade_time')

       

        ## get the most recent screenshot to display on the trade.

        children_with_screen_shot = children.exclude(screen_shot=None).exclude(screen_shot='').order_by('-trade_time')


        if len(children_with_screen_shot) > 0:
            #we have a child screenshot to prioritize
            ss = children_with_screen_shot[0]

            screen_shots = {'screen_shot': ss.screen_shot, 'screen_shot2': ss.screen_shot2, 'screen_shot3': ss.screen_shot3}


        elif trade.screen_shot != None:
            # no child screenshots, use root trade screenshot 
            screen_shots = {'screen_shot': trade.screen_shot, 'screen_shot2': trade.screen_shot2, 'screen_shot3': trade.screen_shot3}
            

        else:
            screen_shots = {'screen_shot': None, 'screen_shot2': None, 'screen_shot3': None}


        children_objs = [] 
        has_mother_trade = False 
 
        try:
        
            if trade.mother_trade:
                has_mother_trade = True 
 
                try:
  
                    mo_trade = Trade.objects.get(id=trade.mother_trade.id) 
         # i think this is causing us to add an extra trade when not neceeded
                   # children_objs.append({'legs': None, 'trade': mo_trade  } ) 
                except Exception as e:
                    print("couldn't locate mother trade, must have been deleted")

        except Exception as e:
            print("exception geting mother_trade of trade, " + str(e))

        if len(children) > 0 and has_mother_trade is False:

            closing_trade_date = None

            for child in children:
            
                child_leg_list = []
                child_legs = TradeLegs.objects.all().filter(trade=child)
                for child_leg in child_legs:
                    child_leg_list.append(child_leg)

                children_objs.append({'legs': child_leg_list, 'trade': child })
 
                if child.trade_type.upper() == 'CLOSING TRADE':
                    closing_trade_date = child.trade_time


                    if child.profit is not None:
                        trade.profit = child.profit
                        trade.save()

                else:
                    pass


            trade_objs.append({'legs': leg_list, 'exit_date': closing_trade_date, 'trade': trade, 'child_trades': children_objs, 'screen_shots': screen_shots })
            

        elif has_mother_trade is False:
            trade_objs.append({'legs': leg_list, 'trade': trade, 'child_trades': children_objs, 'screen_shots': screen_shots })
    
    try:
        trade_objs = sorted(trade_objs, key=lambda trade_obj: trade_obj['child_trades'][0]['trade'].trade_time if (len(trade_obj['child_trades']) > 0) else trade_obj['trade'].trade_time, reverse=True )

    except Exception as e:
        print("error sorting (trade_time key no doubt" + str(e))
    print("process trades complete" ) 
    return trade_objs



def process_positions(trades):
    print("starting process_positions" ) 
    trade_objs = []
    for trade in trades:
        leg_list = []
        legs = TradeLegs.objects.all().filter(trade=trade)

        trade.underlying = chop_futures_underlying(trade.underlying)
        trade.underlying = chop_w_underlying(trade.underlying)

        for leg in legs:

            #leg = serializers.serialize('json', [leg, ])

            leg_list.append(leg)

        children = Trade.objects.all().filter(mother_trade=trade).order_by('trade_time')

       

        ## get the most recent screenshot to display on the trade.

        children_with_screen_shot = children.exclude(screen_shot=None).exclude(screen_shot='').order_by('-trade_time')


        if len(children_with_screen_shot) > 0:
            #we have a child screenshot to prioritize
            ss = children_with_screen_shot[0]

            screen_shots = {'screen_shot': ss.screen_shot, 'screen_shot2': ss.screen_shot2, 'screen_shot3': ss.screen_shot3}


        elif trade.screen_shot != None:
            # no child screenshots, use root trade screenshot 
            screen_shots = {'screen_shot': trade.screen_shot, 'screen_shot2': trade.screen_shot2, 'screen_shot3': trade.screen_shot3}
            

        else:
            screen_shots = {'screen_shot': None, 'screen_shot2': None, 'screen_shot3': None}


        children_objs = [] 
        has_mother_trade = False 
 
        try:
        
            if trade.mother_trade:
                has_mother_trade = True 
 
                try:
  
                    mo_trade = Trade.objects.get(id=trade.mother_trade.id) 
         # i think this is causing us to add an extra trade when not neceeded
                   # children_objs.append({'legs': None, 'trade': mo_trade  } ) 
                except Exception as e:
                    print("couldn't locate mother trade, must have been deleted")

        except Exception as e:
            print("exception geting mother_trade of trade, " + str(e))

        if len(children) > 0 and has_mother_trade is False:

            closing_trade_date = None

            for child in children:
            
                child_leg_list = []
                child_legs = TradeLegs.objects.all().filter(trade=child)
                for child_leg in child_legs:
                    child_leg_list.append(child_leg)

                children_objs.append({'legs': child_leg_list, 'trade': child })
 
                if child.trade_type.upper() == 'CLOSING TRADE' or child.trade_type.upper() == 'EXPIRATION TRADE':
                    closing_trade_date = child.trade_time


                    if child.profit is not None:
                        trade.profit = child.profit
                        trade.save()

                else:
                    pass


            trade_objs.append({'legs': leg_list, 'exit_date': closing_trade_date, 'trade': trade, 'child_trades': children_objs, 'screen_shots': screen_shots })
            

        elif has_mother_trade is False:
            trade_objs.append({'legs': leg_list, 'trade': trade, 'child_trades': children_objs, 'screen_shots': screen_shots })
    
    for trade in trade_objs:
        if trade['trade'].id == 7666:
            print("found trade in trade_objs before sort")

    try:
        trade_objs = sorted(trade_objs, key=lambda trade_obj: trade_obj['child_trades'][0]['trade'].trade_time if (len(trade_obj['child_trades']) > 0) else trade_obj['trade'].trade_time, reverse=True )

    except Exception as e:
        print("error sorting (trade_time key no doubt" + str(e))
    print("process trades complete" ) 
    return trade_objs





def update_closed_trades(trade):

    if trade.trade_type == 'Closing Trade' or trade.trade_type == 'CLOSING TRADE':

        ## get root trade =
        try:
            print("getting mother trade ") 
            #mother_trade =  Trade.objects.get(id=trade.mother_trade)
            mother_trade = trade.mother_trade 
      
        except Exception as e:
            print("no mother trade assigned for the trade, returning none " + str(e))
            return None

        if mother_trade is not None:

            ref_trades = Trade.objects.all().filter(mother_trade=mother_trade)

            for child in ref_trades:

                child.status = 'CLOSED'
                child.save()

            mother_trade.status = 'CLOSED'
            mother_trade.save()
            print("trades updated as closed " )
            return True 

        ## get all trades that have the root trade 
        
        ## mark them status as closed
    print("not a closing trade so don't do updates ") 




def month_str_to_int(month_str):
    month_str = month_str.replace("/", "").strip()

    month_str = month_str.replace("/", "").strip()
    
    for key, value in months_dic.items():
        if value == month_str:
            return str(key)
    print("ERROR converting month string to int: " + str(month_str))

    print("ERROR: " + str(month_str))

    return False 

def generate_month_archive_rows(bucket):
    for key in bucket.keys():
    
        list_of_rows = []
    
        month_list = bucket[key]
        single_row = []
        for month in month_list:
 
            if len(single_row) == 3:
                list_of_rows.append(single_row)
                single_row = []
                single_row.append(month)

            else:
                single_row.append(month)
        if len(single_row) > 0 :
            list_of_rows.append(single_row)

 
        bucket[key] = list_of_rows
    return bucket 




def get_months_with_trades():

    archive_date = datetime(2020, 1, 1	)
    thresh = datetime.now(pytz.utc) - timedelta(days=30)

    all_trades = Trade.objects.filter(trade_time__gte=archive_date).filter(trade_time__lte=thresh)
        
    bucket = {}

    for trade in all_trades:

        trade_date = trade.trade_time
        trade_month_str = months_dic[trade_date.month]
       
        #print("trade date is: " +str(trade_date))
        if trade_date.year not in  bucket.keys():
            #bucket[trade_date.year] =  [trade_date.month]

            bucket[trade_date.year] =  [trade_month_str]
        elif trade_month_str not in bucket[trade_date.year]:
            bucket[trade_date.year].append(trade_month_str)

        


        #print("bucket now is: " + str(bucket))
    
    bucket = generate_month_archive_rows(bucket)
  

    
    return bucket.keys(), bucket

  




def chop_futures_underlying(contract):

    if contract.find("/") != -1:
        return contract[0:3] 
    else:
        return contract 

def chop_w_underlying(contract):
    if contract.find("SPXW") != -1 or contract.find("RUTW") != -1:
        return contract[0:-1]

    return contract



def get_expiration(contract, futures):

	## check if it's a stock or future.

# /GCV19:XCEC 1/100 OCT 19 /OGV19:XCEC
# /ZWZ19:XCBT 1/50 NOV 19 /OZWX19:XCBT
# /ZWZ19:XCBT 1/50 DEC 19 /OZWZ19:XCBT

	if futures:
		pass
		#futures trade parsing here.

	else:
		#equity trade.
		#DIA_111519C261

		contract = contract.split("_")[1]

		if contract.find('C') != -1:
			exp_date = datetime.strptime(contract.split('C')[0], '%m%d%y')

		elif contract.find('P') != -1:
			exp_date = datetime.strptime(contract.split('P')[0], '%m%d%y')

		else:
			return False


	return exp_date




def get_dte(expiration_date):
    

    try:

        today = datetime.now()
    #today = datetime.now(pytz.utc)

        expiration_date = datetime.combine(expiration_date, datetime.min.time())
        delta_obj = expiration_date - today
        return delta_obj.days
    except Exception as e:
        print("Error with get_dte call ")
        return None




def generate_dtes(trade,trade_legs):


    new_dte = None
    prev_dte = None 

    if len(trade_legs) > 0:

        new_dte = get_dte(trade_legs[0].expiration)


    if trade.trade_type == 'Rolling/Adjusting Trade' or trade.trade_type == 'Closing/Adjusting Trade' or trade.trade_type == 'Opening Adjusting Trade':
        
        
        for leg in trade_legs:

            if leg.status == 'OPEN':

                new_dte = get_dte(leg.expiration)

            elif leg.status == 'CLOSED':

                prev_dte = get_dte(leg.expiration)

    return {'new_dte': new_dte, 'prev_dte': prev_dte}



def update_daytrader(customer_id, email, is_daytrader):

    #is_daytrader = is_active_daytrader(customer_id, email)

    try:


        sub = models.Subscriber.objects.get(email=email)
        sub.is_daytrader = is_daytrader
        sub.save()
    except Exception as e:
        a = input('error with update dt' + str(e))
        


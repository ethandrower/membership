from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from django.core import serializers
from .forms import TradeUpdateForm, NewFuturesTradeForm, UpdateProfitForm, ExpirationTradeForm, GhostTradeForm
import requests
import os
import traceback
import ast 
import re
import api.models as models
import json
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.template.loader import render_to_string
from api.utils import process_positions, get_dte, generate_dtes, chop_futures_underlying, chop_w_underlying, get_months_with_trades, update_closed_trades
import api.emailer as emailer
from datetime import datetime, timedelta
from api.clicksend_funcs import send_discount_receipt, send_cancellation_confirmation

import pytz 





def sort_trade_objs(trade_objs):

    sorted_objs = []
#         {'legs': leg_list, 'trade': trade, 'child_trades': children_objs, 'screen_shots': screen_shots })


    sorted(trade_objs, key=lambda trade_obj: trade_obj['child_trades'][0]['trade_time'] )

    return trade_objs 



def check_member_status(email):
    try:

        subscriber = models.Subscriber.objects.filter(email=email)

        return subscriber[0].trade_alerts 

        #return JsonResponse(status=200, data={'pro': subscriber[0].trade_alerts, 'is_annual': subscriber[0].annual })

    except Exception as e:
        print( str(email)  + "no member found, default to Free " + str(e))
        return False


def check_uptime(request):


    return JsonResponse({"alive": True})


def get_positions(request):

    status = request.GET['status'].upper()
    


    if status == 'OPEN':
        email = request.GET['email']

        if check_member_status(email):
            with open('thinkific_cache/open.out', 'r') as f:

                html  = f.read()

                return JsonResponse({'data': html})
        else:
            return JsonResponse({'status': 'failure'})



    else:
        with open("thinkific_cache/closed.out", 'r') as f:
            html = f.read()

            return JsonResponse({'data': html } )

            


    if status == 'OPEN':

        thresh = datetime.now(pytz.utc) - timedelta(days=300000)
    else:
        thresh = datetime.now(pytz.utc) - timedelta(days=30)


    trades = models.Trade.objects.all().filter(status=status, send_alerts=True, trade_time__gt=thresh).order_by('-trade_time')

    trade_objs = [] 
      
    print("Procesing positions") 

    trade_objs = process_positions(trades)

    if status == "OPEN":

        html = render_to_string('thinkific-current-positions.html', {'trade_objs': trade_objs})

    elif status == "CLOSED":
        ## archive_trade_years 
        ## archive_trades = {"2016", [123]}
        #archive_trades_years = ["2012"]
        #archive_trades = {"2012": [1,2,3]}
 
        archive_trades_years, archive_trades = get_months_with_trades()
        html = render_to_string('thinkific-closed-positions.html', {'trade_objs': trade_objs, 'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades })
        
    else:
        return JsonResponse({'error': 'Must choose open or closed'})


    return JsonResponse({'data': html})



def get_alerts_thinkific(request):

    email = request.GET['email']
    print("email found: " + str(email))
    email = email.replace(' ' , '+')

    status = None

    try:
        status = request.GET['open']

    except Exception as e:
        print('showing all alerts')

    try:

        sub = models.Subscriber.objects.filter(email=email.strip())

        if sub[0].trade_alerts:
            with open("thinkific_cache/alerts.out", 'r') as f:
                html = f.read()

            return JsonResponse({'data': html } )           
            

        else:
            return JsonResponse({'status': 'failure', 'message': 'Alerts Disabled'})

    except Exception as e:

        print("exception " + str(e))
        return JsonResponse({'status': 'failure', 'message': 'no subscriber object: ' +str(e)})


def trade_view_home(request):
    trades = models.Trade.objects.prefetch_related('notify_triggers').order_by('-trade_time')
    return render(request, 'home.html', {'trade_objs': trades})


def parse_legs(strikes_array, put_call_array, order_type, initial_strategy, volume, contract_name, ratio_array=None):

    #{"strike":  "volume":  "buy_sell": }
    print("parse legs debug")
    print("strikes array " + str(strikes_array))
    print("put_call_array" + str(put_call_array))
    print("order type: " + order_type)
    print("init strategy " + initial_strategy)
    print("volume " + str(volume))
    print("contract name: " + str(contract_name))
    print("ratio array: {0}".format(ratio_array))


    legs = [] 
    if initial_strategy == 'STRANGLE':
        print("process a strangle")

        for index, strike in enumerate(strikes_array):
            legs.append({"strike": strike, "put_call": put_call_array[index], "volume": volume, "buy_sell": order_type, "contract_name": contract_name})

    elif initial_strategy == 'CUSTOM':
        # custom from BB
        order1 = "BUY" if int(ratio_array[0]) > 0 else "SELL"
        order2 = "BUY" if int(ratio_array[1]) > 0 else "SELL"
        order3 = "BUY" if int(ratio_array[2]) > 0 else "SELL"
        order4 = "BUY" if int(ratio_array[3]) > 0 else "SELL"

        legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) * int(ratio_array[0]) * -1, "buy_sell": order1, "contract_name": contract_name})
        legs.append({"strike": strikes_array[1], "put_call": put_call_array[1], "volume": int(volume) * int(ratio_array[1]) * -1, "buy_sell": order2, "contract_name": contract_name})
        legs.append({"strike": strikes_array[2], "put_call": put_call_array[2], "volume": int(volume) * int(ratio_array[2]) * -1, "buy_sell": order3, "contract_name": contract_name})
        legs.append({"strike": strikes_array[3], "put_call": put_call_array[3], "volume": int(volume) * int(ratio_array[3]) * -1, "buy_sell": order4, "contract_name": contract_name})



    elif initial_strategy == 'STRADDLE':

        if order_type == 'BUY':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "BUY", "contract_name": contract_name})
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[1], "volume": int(volume), "buy_sell": "BUY","contract_name": contract_name })
        

        elif order_type == 'SELL':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "SELL", "contract_name": contract_name})
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[1], "volume": int(volume), "buy_sell": "SELL","contract_name": contract_name })
        

    elif initial_strategy == 'SINGLE':
        if int(volume) > 0:
            buy_sell = 'BUY'
        else:
            buy_sell = 'SELL'
        legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": buy_sell, "contract_name": contract_name})




    elif initial_strategy == 'IRON CONDOR':
        if order_type == 'BUY':
            
            #if buy  then  BUY strike[0],  SELL strike[1], SELL strike[2] BUY strike[3]
            # 337/340/325/312  CALL/PUT
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "BUY", "contract_name": contract_name})
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[1], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[1], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name })

        elif order_type == 'SELL':
            #SELL -2 iron condor   337/340/325/312  CALL/PUT
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) ,  "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[1], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[1], "volume": int(volume) * -1, "buy_sell": "BUY","contract_name": contract_name })

        ### to do, how to handle adjustment trades, and partials and rolls.

    elif initial_strategy =='VERTICAL':
        if order_type =='SELL':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "SELL","contract_name": contract_name})
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1,  "buy_sell": "BUY","contract_name": contract_name })

        if order_type =='BUY':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1 ,  "buy_sell": "SELL","contract_name": contract_name })


### add contracts based on dates. 
#BUY +1 CALENDAR EWZ 100 (Weeklys) 13 DEC 19/29 NOV 19 43 CALL @.48 LMT

    elif initial_strategy == 'CALENDAR':

        contract_name_back = contract_name.split("/")[0]

        contract_name_front = contract_name_back[0:-len(contract_name.split("/")[1])+1] + " " +  contract_name.split("/")[1]

        if order_type == 'BUY':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) * -1,  "buy_sell": "SELL","contract_name": contract_name_front })
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) ,  "buy_sell": "BUY","contract_name": contract_name_back })



        elif order_type =='SELL':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) * -1 ,  "buy_sell": "BUY","contract_name": contract_name_front })
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume)  ,  "buy_sell": "SELL","contract_name": contract_name_back })


    elif initial_strategy == 'BUTTERFLY':


        if order_type == 'BUY':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) ,  "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -2, "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name })
        # buy butterfly  short middle buy wings

        elif order_type == 'SELL':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) ,  "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -2, "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name })


        # Long butterfly   BUY +1 BUTTERFLY EWZ 100 (Weeklys) 29 NOV 19 39/43/47 CALL @2.57 LMT

       # Long 1 39 Call 
       # Short 2 43 Calls 
       # Long 1 47 Call 



    elif initial_strategy =='DBL DIAG':
        # SELL -3 DBL DIAG XRT 100 20 DEC 19/15 NOV 19 43/43/43/41 CALL/PUT/CALL/PUT @1.12 LMT 
        ## BUY +1 DBL DIAG RUT 100 (Weeklys) 29 NOV 19/22 NOV 19 1600/1580/1600/1580 CALL/PUT/CALL/PUT @8.00 LMT
     ## if initial strategy is a DBL DIAG - 
        # SELL -3 DBL DIAG XRT 100 20 DEC 19/15 NOV 19 43/43/43/41 CALL/PUT/CALL/PUT @1.12 LMT 
        ## BUY +1 DBL DIAG RUT 100 (Weeklys) 29 NOV 19/22 NOV 19   1600/1580/1600/1580 CALL/PUT/CALL/PUT @8.00 LMT
     ## RUT 100(Weeklys) 29 NOV 19/22 NOV 19 
     #        20 DEC 19/15 NOV 19

     # BUY +1 STRANGLE /NGQ19:XNYM 1/10000 AUG 19 (Financial) /LNEQ19:XNYM 2.45/3 CALL/PUT @.62 LMT
       # if parsed_tos['initial_strategy'] == 'DBL DIAG':
            # split the contracts. 
        ### TODO
        
        #   pass contract name into the parse legs function and have that function return the legs WITH a contract
        # this wil llet us acocunt for the dbl diag change in contracts.       
        contract_name_front = contract_name.split("/")[0]

        contract_name_back = contract_name_front[0:-len(contract_name.split("/")[1])+1] + " " +  contract_name.split("/")[1]

        #  this is really a roll,  first two strikes are the new position (short),  second two are buys to close old. 
        if order_type =='BUY':
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY", "contract_name":contract_name_front })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[1], "volume": int(volume) , "buy_sell": "BUY", "contract_name": contract_name_front })
            
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[2], "volume": int(volume) * -1 , "buy_sell": "SELL", "contract_name": contract_name_back})
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[3], "volume": int(volume) * -1 , "buy_sell": "SELL", "contract_name": contract_name_back })


        elif order_type=="SELL":
            # open the first two as new position (SELLING)
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL", "contract_name": contract_name_front })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[1], "volume": int(volume) , "buy_sell": "SELL", "contract_name": contract_name_front })
            
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[2], "volume": int(volume) * -1 , "buy_sell": "BUY", "contract_name": contract_name_back })
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[3], "volume": int(volume) * -1 , "buy_sell": "BUY", "contract_name": contract_name_back })



    elif initial_strategy == 'DIAGONAL':
        #BUY +1 DIAGONAL EWZ 100 (Weeklys) 13 DEC 19/29 NOV 19 43.5/43 CALL @.17 LMT
        contract_name_front = contract_name.split("/")[0]

        contract_name_back = contract_name_front[0:-(len(contract_name.split("/")[1])+2)] + " " +  contract_name.split("/")[1]
        contract_name_back = contract_name_back.strip().replace(")", ") ")

        #  this is really a roll,  first two strikes are the new position (short),  second two are buys to close old. 
        if order_type =='BUY':
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1 , "buy_sell": "SELL", "contract_name": contract_name_back })
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY", "contract_name":contract_name_front })

          


        elif order_type=="SELL":
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1 , "buy_sell": "BUY", "contract_name": contract_name_back })
            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL", "contract_name":contract_name_front })

            # open the first two as new position (SELLING)
            

    elif initial_strategy == 'CONDOR':
        if order_type == 'BUY':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "BUY", "contract_name": contract_name})
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) * -1  , "buy_sell": "SELL","contract_name": contract_name })
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name })


#SELL -1 CONDOR DE 100 (Weeklys) 29 NOV 19 172.5/175/177.5/180 CALL @.48 LMT


        elif order_type == 'SELL':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": volume,  "buy_sell": "SELL", "contract_name": contract_name})
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) * -1  , "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[3], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name })


    elif initial_strategy =='BACKRATIO':

        if order_type == 'BUY':
#            BUY +1 1/2 BACKRATIO VIX 100 20 MAY 20 [AM] 12/17 CALL @-1.00 LMT
            # sell  1 12 buy 2 17
            # ratio array [1, 2]

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) * int(ratio_array[0]) * -1, "buy_sell": "SELL","contract_name": contract_name })

            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume)  * int(ratio_array[1])  , "buy_sell": "BUY","contract_name": contract_name })
            

        elif order_type == 'SELL':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) * -1 * int(ratio_array[0])  , "buy_sell": "BUY","contract_name": contract_name })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * int(ratio_array[1])  , "buy_sell": "SELL","contract_name": contract_name })


        # do some kind of volume calculation here for ratio

    elif initial_strategy == "VERITCAL ROLL" or initial_strategy =='VERT ROLL':

        #SELL -6 VERT ROLL SPY 100 15 MAR 19/15 FEB 19 274/277/267/270 CALL @-1.01 LMT
        
        #BUY +2 VERT ROLL AAPL 100 17 JAN 20/20 DEC 19 285/275/265/255 PUT @5.70 LMT



# so in this example, we are rolling the call vertical which WAS:
# February
# SHORT -1 267 Call
# Long +1 270 Call


        #So the roll closed out the Feb piece and entered the March piece. So we end up with
        #Short -1 274 Call
        #Long +1 277 Call
        contract_name_front = contract_name.split("/")[0]

        contract_name_back = contract_name_front[0:-len(contract_name.split("/")[1])+1] + " " +  contract_name.split("/")[1]



        if order_type == 'BUY':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name_front })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name_front })
            
            if len(put_call_array) == 1:
                legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name_back })
                legs.append({"strike": strikes_array[3], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name_back })


            else:

                legs.append({"strike": strikes_array[2], "put_call": put_call_array[2], "volume": int(volume) * -1, "buy_sell": "SELL","contract_name": contract_name_back})
                legs.append({"strike": strikes_array[3], "put_call": put_call_array[3], "volume": int(volume) , "buy_sell": "BUY","contract_name": contract_name_back })



        if order_type == 'SELL':

            legs.append({"strike": strikes_array[0], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name_front })
            legs.append({"strike": strikes_array[1], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "BUY","contract_name": contract_name_front })
        
            if len(put_call_array) == 1:
                legs.append({"strike": strikes_array[2], "put_call": put_call_array[0], "volume": int(volume) * -1, "buy_sell": "BUY","contract_name": contract_name_back })
                legs.append({"strike": strikes_array[3], "put_call": put_call_array[0], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name_back })
      

            else:

                legs.append({"strike": strikes_array[2], "put_call": put_call_array[2], "volume": int(volume) * -1, "buy_sell": "BUY" ,"contract_name": contract_name_back})
                legs.append({"strike": strikes_array[3], "put_call": put_call_array[3], "volume": int(volume) , "buy_sell": "SELL","contract_name": contract_name_back })


    return legs
@csrf_exempt
def update_profit(request):

    try:
        if request.method =='POST':
            print("trade pl info")

            profits = request.POST.getlist('profit')
            trade_ids = request.POST.getlist('trade_id')

            print(type(profits))
            print(str(profits))


            for index, val in enumerate(trade_ids):
                print("index: " +str(index) + " Val: " + str(val))
                if profits[index] != '':
                    print("found submited profit: " + str(profits[index]))



                    

               
                    print("form valid")

                    trade_id  = val
                    profit = profits[index]
                    print("trade id " + str(trade_id))
                    print("profit passed: "  + str(profit))
                    try:
                        trade = models.Trade.objects.get(id=trade_id)
                        trade.profit = profit
                        trade.save()

                    except Exception as e:
                        print("exception getting trade id ")
            
            return redirect('/profit')

        else:

            thresh = datetime.now(pytz.utc) - timedelta(days=40)

            trades = models.Trade.objects.filter(status='CLOSED', trade_time__gt=thresh).order_by('-trade_time')

            trade_objs = []

            for trade in trades:

                form = UpdateProfitForm(initial={'trade_id': trade.id, 'profit': trade.profit})

                trade_obj = {'trade': trade, 'form': form}
                trade_objs.append(trade_obj)


        

            return render(request, 'profit.html',{'trade_objs': trade_objs})



                

    except Exception as e:
        print("exception in update profit view: " + str(e))


def parse_tos_string(trade_str, is_single=False):

       #both
    parsed_tos = {}
#BUY +1 /6EM20:XCME 1/125000 APR 20 (European) /EUUJ20:XCME 1.075 PUT @.0002 LMT  AND  SELL -1 /6EM20:XCME 1/125000 APR 20 (European) /EUUJ20:XCME 1.115 PUT @.0043 LMT
    if is_single:
        l = trade_str.split(" ")
        l.insert(2, "SINGLE")
        trade_str = " ".join(l)


    parsed_tos['order_type'] = trade_str.split(" ")[-1]
    print("Order Type: " + parsed_tos['order_type'])

    #both
    parsed_tos['fill'] = trade_str.split(" ")[-2].strip('@').replace("''",".").replace("\"",".")

    print("Fill price " + str(parsed_tos['fill']))

    #both
    parsed_tos['option_types'] =  trade_str.split(" ")[-3]
    print("option types " + str(parsed_tos['option_types']))

    #both
    parsed_tos['option_types_legs'] = parsed_tos['option_types'].split("/")
    print("option type legs " + str(parsed_tos['option_types_legs']))

    #both
    parsed_tos['strikes'] = trade_str.split(" ")[-4]
    print("Strikes: " + str(parsed_tos['strikes']))

    parsed_tos['strike_legs'] = parsed_tos['strikes'].split("/")
    print("Strike legs: " + str(parsed_tos['strike_legs']))

    parsed_tos['volume'] = trade_str.split(" ")[1]
    parsed_tos['volume'] = parsed_tos['volume'].replace("+", "")

    print("Volume: " + str(parsed_tos['volume'] ))

##  check for ratio spread exists.
    if re.match('[0-9]+/[0-9]+', trade_str.split(" ")[2]) is not None:
        print("ratio spread detected at ratio " + str(trade_str.split(" ")[2] ))
        parsed_tos['ratio_array'] = trade_str.split(" ")[2].split("/") 

    else:
        print("no ratio found")
        parsed_tos['ratio_array'] = None


    parsed_tos['buy_sell'] = trade_str.split(" ")[0]
    if parsed_tos['buy_sell'] == 'BOT':
        parsed_tos['buy_sell'] = 'BUY'

    if parsed_tos['buy_sell'] == 'SOLD':
        parsed_tos['buy_sell'] = 'SELL'

    trade_types = ["STRANGLE", "STRADDLE","CONDOR", "BUTTERFLY", "DIAGONAL", "IRON CONDOR", "CUSTOM",
            "BACKRATIO", "CALENDAR", "DBL DIAG", "VERTICAL", "VERT ROLL", "VERTICAL ROLL", "SINGLE"]



    trade_parsed = trade_str.split(" ")
    print(str(trade_parsed))
    for index in range(0, len(trade_parsed)-3):

        if trade_parsed[index] in trade_types:
            print("found trade at index " + str(index))
            parsed_tos['initial_strategy'] = trade_parsed[index]
            parsed_tos['contracts_str'] = " ".join(trade_parsed[index+1:-4])
            print("contract string extracted full " + str(parsed_tos['contracts_str']))

            break

        elif (trade_parsed[index] + " " + trade_parsed[index+1]) in trade_types:
            print("trade found at index + index +1")
            print(trade_parsed[index] +" " +  trade_parsed[index+1])
            parsed_tos['contracts_str'] = " ".join(trade_parsed[index+2:-4] )
            parsed_tos['initial_strategy'] = trade_parsed[index] + " " + trade_parsed[index+1] 
            break 


    #print("contract string extracted full " + str(parsed_tos['contracts_str']))
     
    return parsed_tos


@csrf_exempt
def new_futures_trade(request):

    try:
        if request.method =='POST':

            print("parse trade info here")

            form = NewFuturesTradeForm(request.POST)

            if form.is_valid():
                

                trade_str = form.cleaned_data['trade']
                trade2_str = form.cleaned_data['trade2']

                trade_type = form.cleaned_data['trade_type']
                iv_percentile = form.cleaned_data['iv_percentile']
                status = form.cleaned_data['status_after_update']
                #expiration = form.cleaned_data['expiration']
                prev_dte = form.cleaned_data['prev_dte']
                new_dte = form.cleaned_data['new_dte']

                is_equity = form.cleaned_data['is_equity']
                print("Is Equity Switch: " + str(is_equity))

                is_double_single = form.cleaned_data['is_double_single']


               

                parsed_trade = parse_tos_string(trade_str, is_double_single)

                contract_name = parsed_trade['contracts_str'] 
                print(contract_name)

                
                if is_equity:
                    underlying = parsed_trade['contracts_str'].split(" ")[0]

                else:

                    underlying = parsed_trade['contracts_str'].split(":")[0]
                print("Underlying detected " + str(underlying))


                trade = models.Trade(trade_type=trade_type, iv_percentile=iv_percentile, 
                                    spread_fill=parsed_trade['fill'], initial_strategy=parsed_trade['initial_strategy'],
                                    buy_sell=parsed_trade['buy_sell'], spread_volume=parsed_trade['volume'], 
                                    tos_string=trade_str, underlying=underlying, status='OPEN')
                trade.save()



## if initial strategy is a DBL DIAG - 
        # SELL -3 DBL DIAG XRT 100 20 DEC 19/15 NOV 19 43/43/43/41 CALL/PUT/CALL/PUT @1.12 LMT 
        ## BUY +1 DBL DIAG RUT 100 (Weeklys) 29 NOV 19/22 NOV 19   1600/1580/1600/1580 CALL/PUT/CALL/PUT @8.00 LMT
     ## (Weeklys) 29 NOV 19/22 NOV 19 
     #        20 DEC 19/15 NOV 19

     # BUY +1 STRANGLE /NGQ19:XNYM 1/10000 AUG 19 (Financial) /LNEQ19:XNYM 2.45/3 CALL/PUT @.62 LMT
       # if parsed_tos['initial_strategy'] == 'DBL DIAG':
            # split the contracts. 
        ### TODO
       # TODO
        #   pass contract name into the parse legs function and have that function return the legs WITH a contract
        # this wil llet us acocunt for the dbl diag change in contracts.       

                parsed_legs  =  parse_legs(strikes_array=parsed_trade['strike_legs'], put_call_array=parsed_trade['option_types_legs'], order_type=parsed_trade['buy_sell'], initial_strategy=parsed_trade['initial_strategy'], 
                                ratio_array=parsed_trade['ratio_array'],  volume=parsed_trade['volume'], contract_name=contract_name)
                
          


                if len(form.cleaned_data['trade2']) > 0:

                    trade.tos_string2= form.cleaned_data['trade2']
                    trade.save()

                    print("Second Trade Submitted, so generate legs for it also...")
                    parsed_trade2 = parse_tos_string(form.cleaned_data['trade2'], is_double_single)
                    
                    contract_name2 = parsed_trade2['contracts_str'] 
                    parsed_trade2_legs = parse_legs(strikes_array=parsed_trade2['strike_legs'], put_call_array=parsed_trade2['option_types_legs'], order_type=parsed_trade2['buy_sell'], initial_strategy=parsed_trade2['initial_strategy'], volume=parsed_trade2['volume'], contract_name=contract_name2, ratio_array=parsed_trade['ratio_array'])
                    

                    for leg in parsed_trade2_legs:
                        parsed_legs.append(leg)


                legs = []
                for parsed_leg in parsed_legs:
                    print("parsed leg " + str(parsed_leg))
            #parse_legs rows{"strike": strike, "put_call": put_call_array[0], "volume": volume, "buy_sell": order_type}
                    new_leg = models.TradeLegs(trade=trade, contract=parsed_leg['contract_name'], strike=parsed_leg['strike'], put_call=parsed_leg['put_call'], volume=parsed_leg['volume'], buy_sell=parsed_leg['buy_sell'], )
                    new_leg.save()

                    legs.append(new_leg)
                print("generate dtes ")
        
                trade.prev_dte = prev_dte 
                trade.new_dte = new_dte 

                trade.save()



                return redirect('/tradedetail?trade='+str(trade.id))
            else:
                print("form errors " + str(form.errors))
                
        else:
            # it's a get so return the form

            form = NewFuturesTradeForm()

            return render(request, 'futures-trade.html',{'form': form})

    except Exception as e:
        print("caught exception in form processing, sending error " + str(e) + str(traceback.format_exc()))
        error_msg = "Tos Futures/Equity FOrm Processing error \n \n " + str(e) + "\n \n" + str(traceback.format_exc()) + "\n \n" + str(trade_str)

        emailer.send_alert( error_msg)

        return render(request, 'futures-trade.html',{'form': form, 'errors': {"text": str(e)}})


@csrf_exempt
def new_ghost_trade(request):
    #for legless trades
    try:
        if request.method =='POST':

            print("parse trade info here")

            form = GhostTradeForm(request.POST)

            if form.is_valid():


                trade_type = form.cleaned_data['trade_type']
                comments = form.cleaned_data['comments']
                status = form.cleaned_data['status_after_update']
                underlying = form.cleaned_data['symbol']
                iv_percentile = form.cleaned_data['iv_percentile']
                initial_strategy = form.cleaned_data['initial_strategy']
                fill = form.cleaned_data['fill']
                trade = models.Trade(trade_type=trade_type, iv_percentile=iv_percentile, 
                                    spread_fill=fill, initial_strategy=initial_strategy, comments=comments,
                                    underlying=underlying, status=status)
                trade.save()


                new_leg = models.TradeLegs(trade=trade, contract=form.cleaned_data['contract'], strike=form.cleaned_data['strike'], put_call=form.cleaned_data['put_call'], volume=form.cleaned_data['volume'],
                 buy_sell=form.cleaned_data['buy_sell'], expiration=form.cleaned_data['expiration'])
                new_leg.save()



                return redirect('/tradedetail?trade='+str(trade.id))
        else:
            form = GhostTradeForm()

            return render(request, 'ghost-trade.html',{'form': form})
    except Exception as e:
        print("caught exception in form processing, sending error " + str(e) + str(traceback.format_exc()))
        error_msg = "Tos Futures/Equity Ghost FOrm Processing error \n \n " + str(e) + "\n \n" + str(traceback.format_exc()) + "\n \n" 

        emailer.send_alert( error_msg)

        return render(request, 'ghost-trade.html',{'form': form, 'errors': {"text": str(e)}})





@csrf_exempt
def trade_update(request):

    #trade = models.Trade.objects.get(id=form.cleaned_data['trade_id'])

    try:
        trade_id = request.GET['trade']
        trade = models.Trade.objects.get(id=trade_id)

    except Exception as e:
        print(str(e ) + " no trade in url")


        try:    
            
            if request.method == 'POST':

        # if notifications are checked:
            #  create a NotifyTrigger object.
            
                form = TradeUpdateForm(request.POST)

                if form.is_valid():

                    print(str(form.cleaned_data))
                    trade_id = form.cleaned_data['trade_id']

            trade = models.Trade.objects.get(id=trade_id)

        except Exception as e:
            print("bad trade id, check your link: " + str(e))
            return JsonResponse({"status": "failed", "comment": "bad trade id, please make sure your link is correct"})


    if request.method == 'POST':

        # if notifications are checked:
            #  create a NotifyTrigger object.
            
        form = TradeUpdateForm(request.POST, request.FILES)

        if form.is_valid():

            print(str(form.cleaned_data))

            


            

           # try:
           #     notify_trigger = models.NotifyTrigger.objects.get(trade=trade)

           # except Exception as e:

           #     print(str(e) + " no notify_trigger yet on post submit so creeate one")
           #     notify_trigger = models.NotifyTrigger(status='NOT SCHEDULED', trade=trade)
           #     notify_trigger.save()

            try:


                if form.cleaned_data['send_alerts'] :
                    # we have a checked alerts box and haven't already started sending.

                    # send alerts
                    print("send alerts!")
  
                    notify_trigger = models.NotifyTrigger(status='NOT SCHEDULED', trade=trade)
                    notify_trigger.status = 'NEW'
                    notify_trigger.save()
                    trade.send_alerts = True

                    alert_time = datetime.now(pytz.utc)
                    trade.trade_time = alert_time
                    

                    trade.save()

                    print(form.cleaned_data['send_alerts'])
                    print(notify_trigger.status)

                try:
                    print("request files: " + str(request.FILES))
 
                    trade.screen_shot =request.FILES['screen_shot']
                    trade.screen_shot2 = request.FILES['screen_shot2']
                    trade.screen_shot3 = request.FILES['screen_shot3']

                except Exception as e:
                    print("no screenshot uploaded, skip it")


                if form.cleaned_data['mother_trade'] != 'Root Trade':

                    mother_trade_str = form.cleaned_data['mother_trade']
                    mother_trade_id = int(mother_trade_str.split(" ")[1])
                    mother_trade = models.Trade.objects.get(id=mother_trade_id)


                    trade.mother_trade = mother_trade 


                #trade.send_alerts = form.cleaned_data['send_alerts']

                trade.iv_percentile = form.cleaned_data['iv_percentile']
                trade.comments = form.cleaned_data['comments']
                trade.trade_type = form.cleaned_data['trade_type']
                trade.initial_strategy =form.cleaned_data['initial_strategy']
                trade.status = form.cleaned_data['status_after_update']
                print("trade status " + form.cleaned_data['status_after_update'])
                trade.tos_string = form.cleaned_data['tos_string']
                trade.tos_string2 = form.cleaned_data['tos_string2']
                trade.prev_dte = form.cleaned_data['prev_dte']
                trade.new_dte = form.cleaned_data['new_dte']

                trade.save()

            except Exception as e:
                print("error trigger alerts " + str(e))


    try:


        notify_trigger = models.NotifyTrigger.objects.get(trade=trade)
        notification_status = notify_trigger.status 
    except Exception as e:
        print("no notify trigger created yet")
        notification_status = 'NOT SCHEDULED'


   
    print("Notification status " + str(notification_status))
    if notification_status == 'NOT SCHEDULED':
        send_alerts = False 
    else:
        send_alerts = True 


    trade_legs = models.TradeLegs.objects.filter(trade__id=trade_id)

    try:

        expiration = trade_legs[0].expiration

    except Exception as e:
        print("no legs found")
        expiration = None 


    if trade.mother_trade is not None:
        mother_trade_id = trade.mother_trade.id 

    else:
        mother_trade_id = None 

    if trade.status == 'CLOSED':
        print("closed trade, so call the update ")

        update_closed_trades(trade) # if the trade has a mother trade specified

    else:
        print(" not a closed trade, so don't update ") 


    form = TradeUpdateForm(test_arg=trade.underlying, initial={'trade_id':trade_id, 
        'comments': trade.comments, 'iv_percentile': trade.iv_percentile, 
        'trade_type': trade.trade_type, 'initial_strategy': trade.initial_strategy,
        'screen_shot': trade.screen_shot, 'screen_shot2': trade.screen_shot2, 'screen_shot3': trade.screen_shot3, 'mother_trade': mother_trade_id,
        'expiration': expiration, 'prev_dte': trade.prev_dte, 'new_dte': trade.new_dte, 'tos_string': trade.tos_string, 'tos_string2': trade.tos_string2, 'status_after_update': trade.status })
    

    return render(request, 'tradedetail.html', {'form': form, 'trade': trade, 'legs': trade_legs, 'trade_id': trade_id, 'notification_status': notification_status, 'iv_percentile': trade.iv_percentile })


@csrf_exempt
def delivery_report(request):

    print("delivery endpoint hit")
    print(request.GET)
    print(str(request.POST))

    message_id = request.POST['message_id'].strip()

    print(message_id)
    status_code = request.POST['status_code']

    try:
        notify_result = models.NotifyResult.objects.get(sms_message_id=message_id)

        if status_code == '201':    
            notify_result.sms_result = True 
            
        elif int(status_code) >= 300:
            notify_result.sms_result = False

        notify_result.sms_error = request.POST['status_text']
        notify_result.save()
        return JsonResponse({})

    except Exception as e:
        print("not found NR object for that message id, skipping" + str(e))
        return JsonResponse( {})







# this is after we dish out the link to BB to login/authenticate us.
# TODO figoure out if we can store the refresh token and use that once the access token expires?

@csrf_exempt
def receive_token(request):

    # requet goes here 
    #https://navigation.drowerconsultants.com/navigation?code=IlCmlrqfQ%2FlMqk2Yfy1Rc3TPJAA2yEPsD13Zw7mcGQUxXR1K%2FsHCdaB03x5xyMHSGm0QY%2BPyZXAyJwYw2laeAGIqBLqRZ3pjFeERmKKOOy7OQTqsCPwy8yc79NhwxSjxGDlHTw0grGxL1AQmsCEj3OkavxgWtI6DzJhTLu71mh26he%2FyEfPbepU0j1ntlWiPVeTHk8vCxyd7GF%2F8bUf6hr%2FPLgzLni4UeCbhuBE%2FGupCR8DICmJXOzg4LIZgP4n3Sy%2Bqeyb7WqoouYGCjIYEVjhK29ZTEAZ779gHapgzE4RnYF2r8YhQeEO8rXnnOR03ghGbJ49vQWZ7mYBlR3bXKPsoWxyEvIXB%2F%2BwgfObErXnC4dgx0zeibsQDZprzMSSYC0Bu79463LgeTfL2dSBNqQb18VirkT4wJpc3nDBk64KidORGUShnuJBDQwx100MQuG4LYrgoVi%2FJHHvl8el68q48HHhD1U4RMu0hf%2BOATnWp7xypNpYUQFkNKC3hcc3jPkT1tKYGkDCN8r18TM%2FELFtfqvFaaXasUr7tnVy%2BJwLLjgZVFZ45iY5WByILDhWso20tC1i7EZBRToGUMy%2Bj0sAjZvshyb64bK5bVbzTqLv%2FPUGc%2FQKM7c%2B6O1eo8ZLhylnlL6fAIft7Bmc4zBeMX2%2FMZ%2B9FnLRcrbStf8VLkIdbN9BWCLFB4htZOZZFwGzPJoPD4v5tuANIA6Y%2FF0VyeL%2FMwHKYW1Q8d36vOdBR%2FwcpZ8l5YKhIPwEM7IuLQmim79WzTyv83quzoIwyLGBszixAy1oYfDGr5MkWFvhuS%2F6Ox%2FwGoJtDQB4UWjxnDCt0WAVBz36E32XdAvkFNnf1tvrQvQ%2FwxzyYPeQE6mu2060CKtFTDcJ0GoqXuBVS1mxZIEzqPZxF%2Bf0%3D212FD3x19z9sWBHDJACbC00B75E

    code = request.GET['code']

    # add token to the db

    # logic for rechecking expiration.
    # maybe send email notification with link to 're-up' the token. not sure yet.
    # path, _, query_string = self.path.partition('?')
    #url = request.url.split('?')
    #current_token = parse_qs(query_string)['code'][0]

        #Post Access Token Request
    headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
    data = { 'grant_type': 'authorization_code', 'access_type': 'offline', 'code': code, 'client_id': 'JVTWKIENB6PQZMC6GKXPY7ALEKWRTJJH', 'redirect_uri': 'https://admin.navigationtrading.com/navigation' }
    auth_reply = requests.post('https://api.tdameritrade.com/v1/oauth2/token', headers=headers, data=data)
    
    if auth_reply.status_code == 201 or auth_reply.status_code == 200:


        current_env = os.environ.get('ENVIRONMENT')

        if current_env == 'PROD':
            redirect_uri = 'https://admin.navigationtrading.com/navigation'
        else:
            redirect_uri = 'https://admin.navigationtrading.com/navigation'

            #redirect_uri = 'http://localhost:8000/navigation'

        json_data = json.loads(auth_reply.text)

        print(str(json_data) ) 
        print(type(json_data))
        print(str(json_data.keys()))

        eval = ast.literal_eval(auth_reply.text)
        print("try eval now " + str(eval))
        print(str(eval['access_token'] ))
        
        token_obj = models.AmeritradeAuth.objects.get_or_create(
                environment=current_env,
                redirect_uri=redirect_uri
        )
        print(str(json_data))
        token_obj = models.AmeritradeAuth.objects.get(environment=current_env, redirect_uri=redirect_uri)

        token_obj.access_token = json_data['access_token']
        #token_obj.access_token = "test"
        token_obj.refresh_token = json_data['refresh_token']
      
        token_obj.save()
        return  JsonResponse(data={'status': 'udpated token successfully'})

    else:
        print("error code other than 201, notify admin" + str(auth_reply) )
        print(auth_reply.text)

        return  JsonResponse(status=404, data={'status': 'Error updating token from link, contact admin'})



## this is active
@csrf_exempt
def check_membership(request):
    try:
        email = None 
        email = request.POST['email']

        subscriber = models.Subscriber.objects.filter(email=email)

        return JsonResponse(status=200, data={'pro': subscriber[0].trade_alerts, 'is_annual': subscriber[0].annual, 
                                              'is_sixmonth': subscriber[0].sixmonth, 'cancel_pending': subscriber[0].cancel_pending, 
                                              'clickfunnel_legacy': subscriber[0].clickfunnel_legacy, 'is_daytrader': subscriber[0].is_daytrader  })

    except Exception as e:
        print(str(email) + "no member found, default to Free " + str(e))


        return JsonResponse(status=200, data={'pro': False})



from django.core.management import call_command




### zapier webhok handler ### 
@csrf_exempt
def new_user(request):

    print(str(request.POST)) 

    try:
        phone = request.POST['phone']

    except Exception as e:
        print("no phone passed, create user ")
        phone = None

    try:

        email = request.POST['email']
        lifetime = request.POST['lifetime']
        print("lifetime val: " + str(lifetime))


        email = email.replace(' ', '+')
        try:
            sub = models.Subscriber.objects.get(email=email)
            sub.trade_alerts = True
            sub.lifetime = lifetime
            if lifetime:
                sub.annual = True

            sub.save()
            print("calling sync subscsribers for " + str(email))

            call_command('sync_subscribers --today 1')
            #call_command('sync_subscribers --email ' + email)

        except Exception as e:
            print("no sub objects found (or multiple)")
            subscriber = models.Subscriber(phone=phone, email=email, alerts_email=email )

            subscriber.save()
            print("calling sync subscribers command for " + str(email))
 
            call_command('sync_subscribers --today 1')
        return JsonResponse(status=200, data={'error': None})

    except Exception as e:
        return JsonResponse(status=500, data={'error': str(e)})


@csrf_exempt 
def handle_new_subscriber(request):

    #no res = json.loads(request.text)
    res = json.loads(request.body.decode('utf-8') )

    stripe.api_key = os.environ.get('STRIPE_SECRET')
# no     res = request.json()

    cust_id =  res['data']['object']['customer']
    cust = stripe.Customer.retrieve(cust_id)
        #print(str(cust))
    email = cust['email']
    print("email found : " + str(email))

    try:
        sub = models.Subscriber.objects.get(email=email)
        sub.trade_alerts = True
        sub.is_daytrader = True
        sub.save()
        print("updating trade alerts set to true") 

    except Exception as e:
        print("no sub objects found (or multiple)")
        subscriber = models.Subscriber(trade_alerts=True, email=email, alerts_email=email, is_daytrader=True )

        subscriber.save()
    return JsonResponse(status=200, data={'error': None})


    print("email: " + str(email))

    
    return JsonResponse(status=200, data={'error': None})






@csrf_exempt
def handle_leg_update(request):

    leg_id = request.POST['leg']
    status = request.POST['enabled']
    print("status from post: " + str(status)
   )

    if status == 'true':
        status = True
    else:
        status = False 
 

    leg = models.TradeLegs.objects.get(id=leg_id)
    leg.alerts_enabled = status  
    leg.save()



    return JsonResponse({"status": "success", "message": "updated leg status"})


@csrf_exempt
def handle_new_expiration_trade(request):

    ## if it's a post

    if request.method == "POST":

           #  create a NotifyTrigger object.
            
        form = ExpirationTradeForm(request.POST)
        if form.is_valid():



            if len(form.cleaned_data['trade_id']) > 0 :

                trade_id = form.cleaned_data['trade_id']
                print("getting trade id from input text field")
                
            else:
                print(str( form.cleaned_data) )

                trade_id = form.cleaned_data['exp_trade'].split(" ")[1]
            
            print("trade id passed to new exp trade: " + str(trade_id))


            try:
                trade = models.Trade.objects.get(id=trade_id)

                trade_legs = models.TradeLegs.objects.filter(trade=trade)

                # set root expiration as CLOSED  # bb trello request.
                trade.status = "CLOSED"
                trade.save() 


                #clone trade and create new one
                trade.id = None
                
                trade.save()

                trade.trade_type = 'Expiration Trade'
                root_trade = models.Trade.objects.get(id=trade_id)
                trade.mother_trade = root_trade

                trade.save()
                
                # clone each leg and assign new FK to cloned trade.
                for leg in trade_legs:
                    leg.id = None

                    leg.trade = trade 
                    leg.save()

            except Exception as e:
                print("Exception creating Expiration Trade: " + str(e))


        return redirect('/tradedetail?trade='+str(trade.id))

    else:
        form = ExpirationTradeForm()
        print("form created")

        return render(request, 'expiration-form.html', {'form': form })



from api.utilities.thinkific_utils import set_expiration_thinkific

@csrf_exempt
def cancel_request(request):

    try:
        email = request.POST['email']
        print("email: " + email )
        stripe.api_key = os.environ.get('STRIPE_SECRET')

        ## first get the subscriber and their valid subscriptions 

        try:
            sub = models.Subscriber.objects.get(email=email)
        except Exception as e:

            return JsonResponse(status=500, data={"error": "No subscriber or multiple found"})

        subs = stripe.Subscription.list(
                limit=100, customer=sub.stripe_id, status='active' )

        trials = stripe.Subscription.list(
                limit=100, customer=sub.stripe_id, status='trialing' )

        for trial in trials['data']:
            subs['data'].append(trial)

        for index, subscription in enumerate(subs['data']):
            if  subscription['plan']['interval'].lower() == 'month' and subscription['plan']['interval_count'] == 1:
                    interval  = 'monthly'
                    
            elif subscription['plan']['interval'].lower() == 'month' and subscription['plan']['interval_count'] == 6:
                interval = '6month' 

            else:
                interval = 'annual'


        ## update stripe subscription to cancel_at_cancel_at_period_end = True cancel_at_period_end
            stripe.Subscription.modify(subscription['id'], cancel_at_period_end= True)
            ## update subscriber by email
            sub.cancel_pending = True
            sub.save()


        ## thinkific now.
        period_end = sub.period_end 

        try:
            set_expiration_thinkific(email, period_end, interval)
        
        except Exception as e:
            print("Exception setting thinkific exiration " + str(e))

        try:

            send_cancellation_confirmation(email)

        except Exception as e:
            print("exception sending cancellation confirmation email " + str(e))



        return JsonResponse(status=200, data={'status': 'success'})
   
    except Exception as e:
        print(str(e))






@csrf_exempt
def discount_request(request):

    try:
        user = request.POST['email']
        offer = request.POST['offer']

        res = emailer.send_discount(user, offer)
        print(str(res))

        if res:
            return JsonResponse({"status": "success", "message": "offer sent to support team"})
        else:
            return JsonResponse({"status": "failure", "message": "issue sending to support"})

    except Exception as e:

        return JsonResponse({"status": "failure", "message": "issue sending " + str(e) })




    ## get the trade ID, get all legs and then return a trade view. 

    ## or create a new trade and go to the update trade page.

import stripe


@csrf_exempt
def price_plan_discount(request):

    # get Subscriber from get email.

    monthly_discount_plan = os.environ.get('MONTHLY_STRIPE_DISCOUNT_PLAN')
    annual_discount_plan = os.environ.get('ANNUAL_STRIPE_DISCOUNT_PLAN')
    stripe.api_key = os.environ.get('STRIPE_SECRET')

    print("Monthly Plan: " + str(monthly_discount_plan))
    print("Annual: " + str(annual_discount_plan))

    email = request.POST['email']
    print("email received: " + email)

    subs = models.Subscriber.objects.filter(email=email)
    
    for sub in subs:
        if sub.trade_alerts:
            print("Trade alert enabled, apply discount attempting...")
            is_annual = sub.annual
            if is_annual:
                plan = annual_discount_plan
            else:
                plan = monthly_discount_plan

            print("Plan to set: " + str(plan))

            cust = stripe.Customer.retrieve(sub.stripe_id)
            print("Customer Retrieved from Stripe: " + str(cust))

            subscriptions = cust['subscriptions']['data']
            print("Subscriptions found: " + str(len(subscriptions)))

            for sub in subscriptions:
                print("Sub in subscriptions: " + str(sub))
                subscription = stripe.Subscription.retrieve(sub['id'])
                print("Modifying plan now: " + str(subscription['id']))
                res = stripe.Subscription.modify(
                    subscription['id'],
                    cancel_at_period_end=False,
                    items=[{
                        'id': subscription['items']['data'][0].id,
                        'plan': plan

                    }]

                )
                print("Status of Modification: " + str(res))

            
            send_discount_receipt(email)

            return JsonResponse({"status": "success"})
        else:
            return JsonResponse(status=500, data={'error': str("No pro subscriber found: " )})


    #  send email confirmation - plus resposne back to thinkific
    





from api.models import Trade
import requests
import os
import traceback
import ast 
import re
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.template.loader import render_to_string
from api.utils import month_str_to_int, get_months_with_trades, process_positions_archives, process_positions




def archive_page(request):

    ## get trades by request filter (url param)
    year = request.GET['year']
    month = request.GET['month']

    month_year_str = month.replace("/", "")  + " " + year 

    month = month_str_to_int(month)
    print("converted month" + str(month))

    #trades = Trade.objects.filter(trade_time__month=month , trade_time__year=year )
    
    trades = Trade.objects.filter( trade_time__year=year).filter( trade_time__month=month)
#    trades = Trade.objects.filter( trade_time__year=year)

    # for trade in trades:
    # 	print("Trade Month: " + str(trade.trade_time.month))
    print(" Trades Found " + str(len(trades)))


    trade_objs = process_positions(trades)

    archive_trades_years, archive_trades = get_months_with_trades()

    ### we should generate the thinkific clsoed trade template here

    trade_html = render_to_string('thinkific-alerts-list.html', {'trade_objs': trade_objs,  'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades, 'month_year_str': month_year_str })


    #trade_html = render_to_string('thinkific-closed-positions.html', {'trade_objs': trade_objs, 'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades })
    ## show them with closed trades template
    return render(request, 'archives.html', {'trade_html': trade_html } )
    

def archive_page_closed(request):

    ## get trades by request filter (url param)
    year = request.GET['year']
    month = request.GET['month']

    month_year_str = month.replace("/", "") + " " + year 

    month = month_str_to_int(month)
    print("converted month" + str(month))

    #trades = Trade.objects.filter(trade_time__month=month , trade_time__year=year )

    trade_objs = [] 
      
    print("Procesing positions") 

    
    trades = Trade.objects.filter( trade_time__year=year, trade_type='CLOSING TRADE').filter( trade_time__month=month)


#    trades = Trade.objects.filter( trade_time__year=year)

    # for trade in trades:
    #   print("Trade Month: " + str(trade.trade_time.month))
    trade_objs = process_positions_archives(trades)

    archive_trades_years, archive_trades = get_months_with_trades()

    trade_html = render_to_string('thinkific-closed-positions.html', {'trade_objs': trade_objs, 'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades, 'month_year_str': month_year_str })
    ## show them with closed trades template
    return render(request, 'archives.html', {'trade_html': trade_html } )
    

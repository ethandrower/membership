




import requests
import os
import traceback
import ast 
import re
import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.template.loader import render_to_string
from api.utils import month_str_to_int, get_months_with_trades, process_positions_archives, process_positions

import api.models as models
import api.emailer as emailer
from datetime import datetime, timedelta
from api.clicksend_funcs import send_discount_receipt, send_email
import stripe
import pytz 

import urllib 
import hashlib
import hmac
import base64

def store_auth(nonce, sig):
    pass
    try:
        auth_obj = models.SSOAuth.objects.get(nonce=nonce)
        auth_obj.sig = sig
        auth_obj.save()
    except Exception as e:

        auth_obj = models.SSOAuth(nonce=nonce, sig=sig)
        auth_obj.save()
    return True


def handle_discourse_1(request):

    sso_payload = request.GET['sso']
    sig = request.GET['sig'] 
    # payload = nonce=abcd   and it's base64 encoded
    
    # validate teh signature 
    # sha256 of sso_secret, payload  == sig  (sig is hex encoded)
    
    
    
    secret = os.environ.get('SSO_SECRET')
    
    store_auth(sso_payload, sig)


    message = bytes(sso_payload, "utf8")
    secret = bytes(secret, "utf8")
    
    signature = base64.b64encode(hmac.new(secret, message, digestmod=hashlib.sha256).digest())
    signature_regular = hmac.new(secret, message, digestmod=hashlib.sha256).digest()
    print("Our computeed sig: " + str(signature))
    print("regular sig: "  +    str(signature_regular) )
    print( "Fromhex discourse sig " + str( bytes.fromhex(sig)))
    print("Sig from discourse " + sig )
    valid = signature_regular ==  bytes.fromhex(sig) 
    print("Match: " + str(valid))
    if valid:
        return redirect('https://members.navigationtrading.com/pages/discourse-authenticate?nonce='+ sso_payload)   
    else:
        return JsonResponse(status=500, data={'status': 'signature mismatch'})
 
    pass
    # here we store the nonce and some other shit 
    
    ## then we redirect to a custom thinkific page.

from urllib.parse import parse_qs


@csrf_exempt 
def handle_discourse_2(request):
    
    nonce = request.POST['nonce']
    print("Nonce passed: " + str(nonce))
    nonce_decode = base64.b64decode(nonce).decode("utf8")
    obj = parse_qs(nonce_decode)
    print(obj['nonce'])

    nonce_parsed = obj['nonce'][0] 
 
    print("nonce decoded: " + str(nonce_decode))

    email = request.POST['email']
    print("Email: " + str(email))

    thinkific_id = request.POST['thinkific_id']
    print("thinkific id: " + str(thinkific_id))

    secret = os.environ.get('SSO_SECRET')
    # first verify the user by email and thinkific id 
    try:
        subs = models.Subscriber.objects.filter(email=email)
        
        if len(subs) > -1: # setting this as always true to include freemembers 
            # now we need to build our second payload 

            auth_obj = models.SSOAuth.objects.get(nonce=nonce)
            sig = auth_obj.sig 
            payload = "nonce="+ nonce_parsed
            payload += "&email=" + email
            payload += "&external_id=" + thinkific_id 
            if len(subs) > 0 and subs[0].trade_alerts:
                payload+="&groups=ProGroup"


            # base64 encode this. 
            payload = bytes(payload, "utf8")

            b64_payload = base64.b64encode(payload) 
            print("Type of b64 payload " + str(type(b64_payload)))

             # 256 hash of payload using sso_secret as key 
            
            secret = bytes(secret, "utf8")
            print("url encode this string")
             
            b64_payload_url =  urllib.parse.quote(b64_payload.decode("utf8"))
            print("b64 url encoded: " + str(b64_payload_url))

#            b64_payload = bytes(b64_payload.decode("utf8"), "utf8") 


            # didnt work b64_payload = bytes( urllib.parse.quote(b64_payload).strip() , "utf8") 

            
            #b64_payload = bytes(b64_payload, "utf8").strip()


            print("Now make 256 hash of b64 payload and secret: " ) 
             
            sig  = hmac.new(secret, b64_payload, digestmod=hashlib.sha256).digest()
            
            print("sig processed" ) 

            print(type(sig))

            print("Sig is " + str(sig.hex()   ) ) 


            #encoded_new_payload = new_payload 
            #encoded_new_payload = urllib.parse.urlencode(new_payload)
            #f = {"sso": new_payload, "sig": sig}
            #qs = urllib.parse.urlencode(f)

            ### return this url string 
            #url =  "https://forum.navigationtrading.com/session/sso_login?"+ qs 
            url =  "https://forum.navigationtrading.com/session/sso_login?sso=" + b64_payload_url + "&sig=" + sig.hex() 
            return JsonResponse({"success": True, "url": url}) 



    except Exception as e:
        print("error " + str(e))






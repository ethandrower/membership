
import requests
import os
import traceback
import ast 
import re
import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.template.loader import render_to_string
from api.utils import month_str_to_int, get_months_with_trades, process_positions_archives, process_positions

import api.models as models
import api.emailer as emailer
from datetime import datetime, timedelta
from api.clicksend_funcs import send_discount_receipt, send_email
import stripe
import pytz 



@csrf_exempt
def discount_request(request):

    try:
        user = request.POST['email']
        offer = request.POST['offer']

        res = emailer.send_discount(user, offer)
        print(str(res))

        if res:
            return JsonResponse({"status": "success", "message": "offer sent to support team"})
        else:
            return JsonResponse({"status": "failure", "message": "issue sending to support"})

    except Exception as e:

        return JsonResponse({"status": "failure", "message": "issue sending " + str(e) })


@csrf_exempt
def schedule_reminder(request):

    try:
        email = request.POST['email']

        rem = models.Reminder(email=email)
        rem.save()
        send_email(email, 'reminder-receipt.html')

        return JsonResponse({"status": "success"})

    except Exception as e:

        return JsonResponse({"status": "failure", "msg": str(e)})


    ## get the trade ID, get all legs and then return a trade view. 

    ## or create a new trade and go to the update trade page.




@csrf_exempt
def price_plan_discount(request):

    # get Subscriber from get email.

    monthly_discount_plan = os.environ.get('MONTHLY_STRIPE_DISCOUNT_PLAN')
    annual_discount_plan = os.environ.get('ANNUAL_STRIPE_DISCOUNT_PLAN')
    sixmonth_discount_plan = os.environ.get('SIX_MONTH_STRIPE_DISCOUNT_PLAN')

    stripe.api_key = os.environ.get('STRIPE_SECRET')

    print("Monthly Plan: " + str(monthly_discount_plan))
    print("Annual: " + str(annual_discount_plan))

    email = request.POST['email']
    print("email received: " + email)

    subs = models.Subscriber.objects.filter(email=email)
    
    for sub in subs:
        if sub.trade_alerts:
            print("Trade alert enabled, apply discount attempting...")
            is_annual = sub.annual
            if is_annual:
                plan = annual_discount_plan
            
            elif sub.sixmonth:
                plan = sixmonth_discount_plan 

            else:
                plan = monthly_discount_plan

            print("Plan to set: " + str(plan))

            cust = stripe.Customer.retrieve(sub.stripe_id)
            print("Customer Retrieved from Stripe: " + str(cust))

            subscriptions = cust['subscriptions']['data']
            print("Subscriptions found: " + str(len(subscriptions)))

            for sub in subscriptions:
                print("Sub in subscriptions: " + str(sub))
                subscription = stripe.Subscription.retrieve(sub['id'])
                print("Modifying plan now: " + str(subscription['id']))
                res = stripe.Subscription.modify(
                    subscription['id'],
                    cancel_at_period_end=False,
                    items=[{
                        'id': subscription['items']['data'][0].id,
                        'plan': plan

                    }]

                )
                print("Status of Modification: " + str(res))

            
            send_discount_receipt(email)

            return JsonResponse({"status": "success"})
        else:
            return JsonResponse(status=500, data={'error': str("No pro subscriber found: " )})

@csrf_exempt
def cancel_now(request):

    stripe.api_key = os.environ.get('STRIPE_SECRET')
    email = request.POST['email']

    zap_url = 'https://hooks.zapier.com/hooks/catch/2067672/o6rhfqo/'

    res = requests.post(zap_url, json={'email': email } )

    # cancel stripe subs. 
    subs = models.Subscriber.objects.filter(email=email)
    
    for sub in subs:
        if sub.trade_alerts:
            print("Trade alert enabled, apply discount attempting...")
            is_annual = sub.annual
            if is_annual:
                plan = annual_discount_plan
            else:
                plan = monthly_discount_plan

            print("Plan to set: " + str(plan))

            cust = stripe.Customer.retrieve(sub.stripe_id)


    pass

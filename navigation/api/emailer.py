
import requests

import clicksend_client
from clicksend_client.rest import ApiException
from clicksend_client import  SmsMessage
from clicksend_client import EmailRecipient
from clicksend_client import EmailFrom
import os
import ast

configuration = clicksend_client.Configuration()
configuration.username = os.environ.get('CLICKSEND_USERNAME')
configuration.password = os.environ.get('CLICKSEND_KEY')


def send_new_auth(link):

    email_text = """Click the Link and update your token  \n \n
          Status: {0} \n
 

           """.format(str(link)   ) 
    #print("returning false before sending full auth link")

    #return False 
    return requests.post(
        "https://api.mailgun.net/v3/speechbytes.com/messages",
        auth=("api", "key-1a7ed853de9ea4a6f1fc43d160063471"),
        data={"from": "Navigation Trading <info@navigationtrading.com>",
              "to": ["ethandrower@gmail.com", "info@navigationtrading.com"],
              "subject": "Token Request TD Ameritrade",
              "text": email_text})


def send_phone_reminder(email, text):

    email_text = text
    
    api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))

    email_recipient=EmailRecipient(email=email,)
    
    email_from=EmailFrom(email_address_id=613,name='Navigation Trading')



    email = clicksend_client.Email(to=[email_recipient],
                          _from=email_from,
                          subject="Update your phone number for trade alerts",
                          body=email_text,
                          ) 


    try:
        # Send sms message(s)
        api_response = api_instance.email_send_post(email)
        print(api_response)
        # create notify result if doesn't exist.
        print(type(api_response))
        api_response = ast.literal_eval(api_response)

        print(api_response['data'])
        


    except Exception as e:
        print(str(e))





    #return requests.post(
        # "https://api.mailgun.net/v3/speechbytes.com/messages",
        # auth=("api", "key-1a7ed853de9ea4a6f1fc43d160063471"),
        # data={"from": "Ethan Drower <mailgun@speechbytes.com>",
        #       "to": [email, ""],
        #       "subject": "Update your phone number for trade alerts",
        #       "html": email_text})



def send_trade_alert_mailgun(message_html, address ):

    # email_text = """Exception in System  \n \n
    #       Status: {0} \n


    #        """.format(str(message)   ) 


    return requests.post(
        "https://api.mailgun.net/v3/speechbytes.com/messages",
        auth=("api", "key-1a7ed853de9ea4a6f1fc43d160063471"),
        data={"from": "Ethan Drower <mailgun@speechbytes.com>",
              "to": [address, ""],
              "subject": "Navigation Trade Alert (mailgun)",
              "html": message_html})





def send_alert(message):
    email_text = """Exception in System  \n \n
          Status: {0} \n
 

           """.format(str(message)   ) 


    return requests.post(
        "https://api.mailgun.net/v3/speechbytes.com/messages",
        auth=("api", "key-1a7ed853de9ea4a6f1fc43d160063471"),
        data={"from": "Ethan Drower <mailgun@speechbytes.com>",
              "to": ["ethan.drower@toptal.com", ""],
              "subject": "Navigation Trading Error",
              "text": email_text})


def send_discount(email, discount):

    email_text = """User Cancellation, Discount Request \n \n
                User Email: {0} \n
                Requested Billing: {1} \n 
                \n
                """.format(str(email), str(discount))
    return requests.post(
        "https://api.mailgun.net/v3/speechbytes.com/messages",
        auth=("api", "key-1a7ed853de9ea4a6f1fc43d160063471"),
        data={"from": "Ethan Drower <mailgun@speechbytes.com>",
              "to": ["ethan.drower@toptal.com", "support@navigationtrading.com"],
              "subject": "Navigation Trading Discount Request",
              "text": email_text})


import requests
from datetime import datetime 
import os
import api.models as models
from api.clicksend_funcs import gen_email
from api.utils import get_dte, generate_dtes, chop_futures_underlying, chop_w_underlying


api_key = os.getenv('DISCOURSE_API_KEY')
api_user = os.getenv('DISCOURSE_API_USER')
forum_url = os.getenv('DISCOURSE_URL')
headers = {
        'Api-Key':'dbd124ef74f84ee1920f277ddaf79e2474c8d68ae4b8567af81d54f122f72338',
        'Api-Username': 'Navigationtrading'
}

def post_to_discourse(trade):

    if trade.tos_string2 is None:
        trade.tos_string2 = ""
  
    trade.underlying = chop_futures_underlying(trade.underlying)
        
    trade.underlying = chop_w_underlying(trade.underlying)

    post_title = "NavigationALERT: {0} - {1} - {2} - {3}"\
        .format(trade.underlying, 
                trade.initial_strategy,
                trade.trade_type, 
                trade.trade_time.strftime('%d-%b-%Y') 

        )


    text = """TRADE DATE - {0} \n \n {1} \n {2} \n {3} \n TRADE COMMENTS \n {4} 

     """.format(trade.trade_time.strftime('%d-%b-%Y'), trade.trade_type.upper(),  trade.tos_string, trade.tos_string2, trade.comments)

  
    # trade_legs = models.TradeLegs.objects.filter(trade=trade, alerts_enabled=True)

    # legs = "Trade LEgs here"

    # email_msg = gen_email(trade, trade_legs)

    data = {
        "title": post_title,
        "category": 7,
        "raw": text
    }

    res = requests.post(forum_url + '/posts', headers=headers, json=data)
    


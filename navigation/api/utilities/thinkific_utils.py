
import requests
from datetime import datetime 
import os

def get_course_name(interval):
    
    if interval == 'monthly':
        return  'Pro Membership (Monthly)'
    elif interval == 'sixmonth' or interval == '6month':
        return 'Pro Membership (6 Months)'
    elif interval == 'annual':
        return 'Pro Membership (Annual)'
    else:
        course_name = 'Pro Membership (6 Months)'



def set_expiration_thinkific(email, exp_date, interval):

    course_name = get_course_name(interval)

    url = "https://api.thinkific.com/api/public/v1/enrollments"

    query_string =  {"query[email]": email}

    thinkific_api_key = os.getenv('THINKIFIC_API_KEY', None)

        #'X-Auth-API-Key': '77c237f14cd0de9ca8a53bdb073563af',

    headers = {
        'X-Auth-API-Key': thinkific_api_key,
        'X-AUTH-Subdomain': 'navigationtrading'

    }
    res = requests.get(url, headers=headers, params=query_string)

    if res.status_code < 299:
        print('success in get enrollments')

    #print(str(res.json()))

    items = res.json()
    for course in items['items']:

        if course['course_name'] == course_name:
            print("Found pro member enrollment " + str(course['id']))
            now = str(datetime.now() ) 
            

            update_url = 'https://api.thinkific.com/api/public/v1/enrollments/' + str(course['id'])
            values = { "expiry_date": str(exp_date),   "is_free_trial": False, "started_at": now, "activated_at": now }
        
            res2 = requests.put(update_url, headers=headers, data=values)
            #print(str(res2))
            print(str(res2))

            print(str(res.text))

    return res.json()



def enroll_thinkific(email, interval):
    

    # query user by email 
    url = "https://api.thinkific.com/api/public/v1/users"

    query_string =  {"query[email]": email}

    thinkific_api_key = os.getenv('THINKIFIC_API_KEY', None)

        #'X-Auth-API-Key': '77c237f14cd0de9ca8a53bdb073563af',
    headers = {
        'X-Auth-API-Key': thinkific_api_key,
        'X-AUTH-Subdomain': 'navigationtrading'

    }
    res = requests.get(url, headers=headers, params=query_string)

    users = res.json()
    print(str(users))
    if users['items']:
        print("found user")
        user_id = users['items'][0]['id']
    else:
        print("error getting user id ")
        return {"success": False, "msg": "could not find that user " + email }



    ## then determine which course to enroll in. 
    #course_name = get_course_name(interval)

    if interval == 'monthly':
        course_id = 541127
    elif interval == 'sixmonth':
        course_id = 619497
    elif interval == 'annual':
        course_id = 568286

    data = {

        "course_id": course_id,
        "user_id": user_id,
        "activated_at": "2018-01-01T01:01:00Z"
    }

    url = "https://api.thinkific.com/api/public/v1/enrollments"
    res = requests.post(url, headers=headers, json=data )
    
    if res.status_code < 299:
        return {"success": True }

    else:
        return {"success": False, "msg": " error adding enrollment "} 




def new_user_thinkific(user):

    url = "https://api.thinkific.com/api/public/v1/users"
    headers = {
        'X-Auth-API-Key': '77c237f14cd0de9ca8a53bdb073563af',
        'X-AUTH-Subdomain': 'navigationtrading'
    }

    data = {
            "first_name": user['first_name'],
            "last_name": user['last_name'],
            "email": user['email'],
            "password": "ironduck678",
            "custom_profile_fields": [
                {
                    "value": user['phone'],
                    "custom_profile_field_definition_id": 17561
                }
            ],
            "send_welcome_email": False,
            "external_id": str(user['external_id'])
    }
    print("data for new user: " + str(data))

     
    res = requests.post(url, headers=headers, json=data )
    if res.status_code < 299:
        return {"success": True }
    else:
        return {"success": False, "msg": "error creating new user " + str(user) + str(res.text) }

    



def create_user_thinkific(email):
    

    url = "https://api.thinkific.com/api/public/v1/enrollments"
    headers = {
        'X-Auth-API-Key': '77c237f14cd0de9ca8a53bdb073563af',
        'X-AUTH-Subdomain': 'navigationtrading'
    }

    data = {"email": email}

    res = requests.post(url, json=data )

    print(res.text)

    return True 



def unenroll_thinkific(email, interval):

    if interval == 'monthly':
        course_name = 'Pro Membership (Monthly)'
    else:
        course_name = 'Pro Membership (6 Months)'

    url = "https://api.thinkific.com/api/public/v1/enrollments"

    query_string =  {"query[email]": email}

    headers = {
        'X-Auth-API-Key': '77c237f14cd0de9ca8a53bdb073563af',
        'X-AUTH-Subdomain': 'navigationtrading'


    }
    res = requests.get(url, headers=headers, params=query_string)

    if res.status_code < 299:
        print('success in get enrollments')

    else:
        pass
        #a = input('problem getting enrollments')

    #print(str(res.json()))

    items = res.json()
    print(str(items))
    #a = input('wait')

    for course in items['items']:

        if course['course_name'] == course_name:
            print("Found pro member enrollment " + str(course['id']))

            update_url = 'https://api.thinkific.com/api/public/v1/enrollments/' + str(course['id'])
            values = { "expiry_date": "2019-04-18T02:18:47.873Z"}
        
            res2 = requests.put(update_url, headers=headers, data=values)
            #print(str(res2))
            print(str(res2))

            print(str(res.text))
            #a = input ('wait after remove') 

    return res.json()

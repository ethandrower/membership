
from django.core.management.base import BaseCommand, CommandError


from clicksend_client.rest import ApiException
from datetime import datetime, timedelta
import time
import api.clicksend_funcs as clicksend_funcs

# Configure HTTP basic authorization: BasicAuth
# create an instance of the API class

### Gets status of delivery for messages + texts and syncs them in the database.

class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('user_id', nargs='*', type=int)

    def handle(self, *args, **options):

    	## query API for 


    	clicksend_funcs.get_sms_message_history()

    	clicksend_funcs.get_email_history()




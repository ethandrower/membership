
from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string


from clicksend_client.rest import ApiException
from datetime import datetime, timedelta
import time
import api.clicksend_funcs as clicksend_funcs
from api.utils import process_positions, get_months_with_trades, chop_futures_underlying, chop_w_underlying
from api.emailer import send_alert

import api.models as models
import pytz




# Configure HTTP basic authorization: BasicAuth
# create an instance of the API class

### Gets status of delivery for messages + texts and syncs them in the database.

def update_trade_alerts_cache():

    trade_objs = [] 
    thresh = datetime.now(pytz.utc) - timedelta(days=30)

    trades = models.Trade.objects.filter(send_alerts=True, trade_time__gt=thresh).order_by('-trade_time')

    for trade in trades:

        try:

            leg_list = []
            legs = models.TradeLegs.objects.all().filter(trade=trade)

            # if legs[0].expiration is not None:

            #     dte = get_dte(legs[0].expiration)
            # else:
            #     dte = None 

            for leg in legs:
                leg_list.append(leg)


           # dte_obj = generate_dtes(trade, legs)
            trade.underlying = chop_futures_underlying(trade.underlying)
            trade.underlying = chop_w_underlying(trade.underlying)
            trade_objs.append({'legs': leg_list, 'trade': trade, })
        except Exception as e:
            emailer.send_alert("Trade Alerts View views " + str(trade.id) + " "  + str(e))

            print("Exception parsing trade, critical: ")

    archive_trades_years, archive_trades = get_months_with_trades()
    html = render_to_string('thinkific-alerts-list.html', {'trade_objs': trade_objs,  'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades})

    with open('thinkific_cache/alerts.out', 'w') as f:
        f.write(html)
    return True



class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('status', nargs='*', type=str)

    def handle(self, *args, **options):

        ## query API for 
        try:

            status = options['status'][0]
            print('status is ' + str(status))

            if status == 'OPEN':
                time.sleep(15)

                thresh = datetime.now(pytz.utc) - timedelta(days=300000)
            else:
                thresh = datetime.now(pytz.utc) - timedelta(days=30)


            update_trade_alerts_cache() 


            all_trades = models.Trade.objects.select_related('mother_trade').all().filter(status=status, send_alerts=True, trade_time__gt=thresh).order_by('-trade_time')

            ## get all objects (and root trades) 
            ## if a trade meets threshold AND has root trade, include root trade
            trades = [] 
            for trade in all_trades:
                trades.append(trade)
                if trade.mother_trade is not None and trade.mother_trade not in trades:
                    if trade.mother_trade.trade_time < thresh:
                        trades.append(trade.mother_trade)


            for trade in trades:
                if trade.id == 7666:
                    print("found trade 7666" )


            trade_objs = [] 
              
            print("Procesing positions") 

            trade_objs = process_positions(trades)

            if status == "OPEN":

                html = render_to_string('thinkific-current-positions.html', {'trade_objs': trade_objs})

                with open('./thinkific_cache/open.out', 'w') as f:
                    f.write(html)

            elif status == "CLOSED":
                ## archive_trade_years 
                ## archive_trades = {"2016", [123]}
                #archive_trades_years = ["2012"]
                #archive_trades = {"2012": [1,2,3]}
                print("before archive trades function call") 
 
                archive_trades_years, archive_trades = get_months_with_trades()
                print("archive_trades in command " + str(archive_trades))

                html = render_to_string('thinkific-closed-positions.html', {'trade_objs': trade_objs, 'archive_trades_years': archive_trades_years, 'archive_trades': archive_trades })
                with open('./thinkific_cache/closed.out', 'w') as f:
                    f.write(html)  

            else:
                return False

        except Exception as e:
            print("exception caught " + str(e))

            send_alert("Update_CACHE Command Exception: " + str(e))




        


            



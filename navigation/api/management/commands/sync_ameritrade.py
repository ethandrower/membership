from __future__ import print_function
import clicksend_client
from clicksend_client.rest import ApiException
from django.core.management.base import BaseCommand, CommandError


from api.ameritrade import get_recent_orders, send_full_auth_email
from api import models
from api.utils import get_expiration, generate_dtes
import api.emailer as emailer

from api.logger import write_log
import traceback
import logging
import pytz
from datetime import datetime 

import api.clicksend_funcs as clicksend_funcs


### pull all subscribers active from our db where clicksend_sync = false



### this isn't ncecessary as a function anymore. 
def update_mother_trade_status():

    trades = models.Trade.objects.all()

    for trade in trades:

        open_legs_count = models.TradeLegs.objects.filter(trade=trade, status='OPEN' ).count()
        if open_legs_count == 0:
            # postiion is closed.  mark the trade as done.

            trade.status = 'CLOSED'
            trade.save() 


def get_trade_type(legs):

# if some are BUY_TO_CLOSE or SELL_TO_CLOSE then its and adjustment
# if ALL are BUY_TO_CLOSE or SELL_TO_CLOSE then it's a closing.
# if some are BUY_TO_OPEN or SELL_TO_Op

    opens = 0
    closes = 0
    try:

        for leg in legs:
            print("processing legs t odetermine trade type " + str(leg)) 
            if leg['instruction'] == 'BUY_TO_OPEN' or  leg['instruction'] == 'SELL_TO_OPEN':
                opens += 1
            elif leg['instruction'] == 'BUY_TO_CLOSE' or leg['instruction'] == 'SELL_TO_CLOSE':
                closes += 1
            else:
                raise Exception('encontered unknown trade leg action' + str(leg))
    except Exception as e:
        print("exception thrown get trade type  " + str(e))

    print( """Opens: {0}  Closes {1}""".format(str(opens), str(closes)))


    if opens == 0:
        return 'CLOSING TRADE'

    if closes == 0:
        return 'OPENING TRADE'

    else:
        return 'ROLLING/ADJUSTING TRADE'


def get_buy_sell_leg(instruction):

    if instruction == 'BUY_TO_OPEN' or instruction == 'BUY_TO_CLOSE':
        return 'BUY'

    elif instruction == 'SELL_TO_OPEN' or instruction == 'SELL_TO_CLOSE':
        return 'SELL'
    else:
        raise Exception('unknown instruction for trade leg ' + str(instruction))


def get_leg_price(trade, leg_id):

    for execution in trade['orderActivityCollection'][0]['executionLegs']:
        if execution['legId'] == leg_id:
            return execution['price']

    return None


def get_put_call(desc):

    return desc.split(' ')[-1].upper()


def get_strike(desc):

    return desc.split(' ')[-2]


def get_buy_sell_trade(trade):
    print("trade buy sell for " + str(trade))
    if trade['orderType'] == 'NET_DEBIT':
        return 'BUY'
    elif trade['orderType'] == 'NET_CREDIT':
        return 'SELL'


def get_mother_trade(symbol):


  #  trade_legs = models.TradeLegs.objects.filter(contract=sybmol, status='OPEN')

    trade = models.Trade.objects.filter(tradelegs__contract=symbol, status='OPEN')



    if len(trade) > 1:
        print("multiple trades found open for this contract, something wrong")

        return None
    elif len(trade) == 1:
        return trade[0]
    return None


def get_underlying(symbol):

    return symbol.split("_")[0]

## add each contact to the list.


class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('user_id', nargs='*', type=int)

    def handle(self, *args, **options):

        #send_full_auth_email()

        logger = logging.getLogger('django')
        logger.info('Command: SYnc Ameritrade ')


        # hit API for trades.
        try:

            orders = get_recent_orders()
            if orders:
                print("orders received " )
                #print(str(orders))

                # logic for this.

                # filter from today's trades only? 

                # list is  orderStrategies []:
                #for trade in orders['orderStrategies']:
                for trade in orders:

                    ## need to create a trade here.
                    if trade['status'] != 'FILLED':
                        print("trade wasn't a fill so skip it ") 
                        continue

                    trade_type = get_trade_type( trade['orderLegCollection'])

                    

                    #might be better way to do this.
                    underlying = get_underlying(trade['orderLegCollection'][0]['instrument']['symbol'])


                    spread_volume = trade['filledQuantity']

                    buy_sell = get_buy_sell_trade(trade)

                    if buy_sell == 'SELL':
                        spred_volume = int(spread_volume) * -1 


                    spread_fill = trade['price']
                    td_id = trade['orderId']
                    initial_strategy = trade['complexOrderStrategyType']

                    ## need to make sure that we haven't already processed this trade
                    ## check by order id and short circuit if exists? 
                    logger.info('Checking for pre-existing trades in db, trade id: ' + str(td_id))


                    existing_trade = models.Trade.objects.filter(td_id=td_id).count()
                    if existing_trade > 0:
                        print("already processed this trade! skip it")
                        logger.info('Existing trade found for id: ' + str(td_id))
                        continue


                    #create new trade here****
                    logger.info('No trade found, create new one for id: ' + str(td_id))
                    now = datetime.now(pytz.utc)
                    central_tz = pytz.timezone('US/Central')
                    trade_time = now.astimezone(central_tz)


                    new_trade = models.Trade(trade_time=trade_time, trade_type=trade_type, initial_strategy=initial_strategy, buy_sell=buy_sell , spread_volume=spread_volume , spread_fill=spread_fill , status='OPEN', td_id=td_id, iv_percentile=0, underlying=underlying, is_api_trade=True)

                    new_trade.save()
                    print("created new trade: " + str(new_trade) + " ID: " + str(td_id))


                    mother_trade = None
                    legs = []
                    for leg in trade['orderLegCollection']:
                        leg_id = leg['legId']


                        symbol = leg['instrument']['symbol']

                        if mother_trade is None:
                            mother_trade = get_mother_trade(symbol)



                        # get mother trade(symbol)
                        description = leg['instrument']['description']

                        expiration = get_expiration(leg['instrument']['symbol'], futures=False)

                        if leg['instruction'] == 'BUY_TO_CLOSE' or leg['instruction'] == 'SELL_TO_CLOSE':
                            # this leg is closing a previous leg somewhere, so find it and update it as closed.
                            leg_status = 'CLOSED'

                        else: # it's opening so we don't have to do anything

                            leg_status='OPEN'

                        buy_sell = get_buy_sell_leg(leg['instruction'])
                        volume = leg['quantity'] 
                        if buy_sell == 'SELL' and int(volume) > 0:
                              # guarantee that sell legs are negative volume                         
                            volume = int(volume) * -1 

                        price = get_leg_price(trade, leg_id)
                        put_call = get_put_call(leg['instrument']['description'])
                        strike = get_strike(leg['instrument']['description'])

                        new_leg = models.TradeLegs(trade=new_trade  , contract_readable=description, contract=symbol, strike=strike , put_call=put_call , volume=volume , price=price , buy_sell=buy_sell , status=leg_status, expiration=expiration )
                        new_leg.save()
                        legs.append(new_leg)

                        print("Created New Leg " + str(new_leg))

                    new_trade.mother_trade = mother_trade
                    
                    
                    dte_obj = generate_dtes(trade,legs) 
                    new_trade.prev_dte = dte_obj['prev_dte']
                    new_trade.new_dte = dte_obj['new_dte']


                    new_trade.save()

                #update_mother_trade_status() # check all outstanding trades and mark any closed.
            write_log(status='SUCCESS', action='AMERITRADE SYNC', trade=None, subscriber=None, comment='Clean run of ameritrade sync')
        except Exception as e:
            print("exception in ameritrade sync " + str(e))
            emailer.send_alert("Error in ameritrade sync command " + str(e ) + "\n \n" + str(traceback.format_exc())) 

            write_log(status='FAILURE', action='AMERITRADE SYNC', trade=None, subscriber=None, comment=str(e) + str(traceback.format_exc()))


#sync ameritrade


#3 orderId  is the linker of multi-leg trades
#  [orderId].1  .2 .3 ... etc. etc.  to indicate multileg


# trade spread potential id

# BUY OPEN XLK oct 18 85 put 


# SELL Closing XLK Sep 20  82 PUT  

# BUY CLOsing  XLK Sep 20 77 PUT  

# SELL Open XLK OCT 18 80  put   

# this was a roll.  spread.

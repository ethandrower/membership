
from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string


from clicksend_client.rest import ApiException
from datetime import datetime, timedelta
import time
import api.clicksend_funcs as clicksend_funcs
from api.utils import process_positions, get_months_with_trades, chop_futures_underlying, chop_w_underlying
from api.emailer import send_alert

import api.models as models
import pytz



class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('status', nargs='*', type=str)

    def handle(self, *args, **options):

        today = datetime.today()
        central = pytz.timezone('US/Central')

        today = central.localize(today)
        
        today = today.astimezone(central)
        today = today + timedelta(days=5)
        
        reminders = models.Reminder.objects.filter(complete=False)

        for rem in reminders:

            if rem.complete is False:

                subscribers = models.Subscriber.objects.filter(email=rem.email)

                for sub in subscribers:
                    if sub.period_end is not None:

                        period_end = sub.period_end
                        period_end = period_end.astimezone(central)
                        if period_end <= today:
                            print("we need to send the reminder email")
                            clicksend_funcs.send_cancellation_reminder(rem.email)
                            

                            rem.complete =True
                            rem.save()

                            continue 

                            


                ## check stripe subscription status.


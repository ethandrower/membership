import stripe

from api import models

from django.core.management.base import BaseCommand, CommandError
import os
import time
from api.logger import write_log
from api.utilities.thinkific_utils import unenroll_thinkific
from api.clicksend_funcs import send_upgrade_receipt
import requests
from datetime import datetime, timedelta
import pytz



def issue_partial_refund(customer_id, subscription_obj):

    charges = stripe.Charge.list(customer=customer_id)
    print(str(charges))

    ## might need to queyr sub object here if we can't pass it.


    # loop through charges and get each invoice number.
    for charge in charges['data']:
        #print(str(charge))
        invoice_id = charge['invoice']
        
        sub_latest_invoice_id = subscription_obj['latest_invoice']
        print("lastest invoice id on sub obj: " + str(sub_latest_invoice_id))
        print("charge invoice: " + str(invoice_id )) 

        if invoice_id == sub_latest_invoice_id:
            print("match!")

            # this is our matching charge,  so calc refund here.
            invoice = stripe.Invoice()

            today = datetime.today()
            period_end = datetime.fromtimestamp( subscription_obj['current_period_end'] )
            period_start = datetime.fromtimestamp( subscription_obj['current_period_start'])
            period_delt = period_end - period_start
            delt = period_end - today

            percent =  delt.days/period_delt.days
            print("Period Delta: " + str(period_delt.days))
            print("today to period_end: " + str(delt.days))

            print("Issue refund percent: " + str(percent))
            print("inital payemnt amount: " + str(charge['amount']))

            amount = int( float(charge['amount']) * percent)
            print("amount to refund: " + str(amount))

            stripe.Refund.create(charge=charge['id'], amount=amount)

            return True


def get_stripe_cust_details(cust_id):

    try:

        cust = stripe.Customer.retrieve(cust_id)
        #print(str(cust))
        email = cust['email']
        try:

            phone = cust['metadata']['phone']
        except Exception as e:
            phone = None

        return {'email': email, 'phone': phone, 'stripe_id': cust_id}
    except Exception as e:

        print("exception getting customer details stripe " + str(e))
        #write_log(status='FAILURE', action='QUERY STRIPE',
        #          comment=cust_id + ' ' + str(e) + str(email))
        return None


#def update_or_create_subscriber(subscriber_info, alerts_on, is_annual, is_sixmonth,  period_end, is_daytrader):

def update_or_create_subscriber(*args, **kwargs):
    print("kwargs " + str(kwargs))
    try:


        subs = models.Subscriber.objects.filter(email=kwargs['subscriber_info']['email'])
        for sub in subs:

            if sub.lifetime is not True:  ## never touch lifetime subscribers.
            
                sub.trade_alerts = kwargs['alerts_on']

                if kwargs['is_annual'] is not None:
                    sub.annual = kwargs['is_annual'] 
                
                if kwargs['is_sixmonth'] is not None:
                    sub.sixmonth = kwargs['is_sixmonth'] 


                chi_tz = pytz.timezone('US/Central')
                if kwargs['period_end'] is not None:
                    period_end = chi_tz.localize(kwargs['period_end'])

                    period_end = period_end.astimezone(chi_tz) 

                    sub.period_end = period_end
     
                sub.stripe_id = kwargs['subscriber_info']['stripe_id']
                #print("stripe id set: " + str(subscriber_info['stripe_id']))
                
                sub.save()
            else:
                sub.trade_alerts = True
                sub.annual = True
                sub.save()

            # if subscriber_info['email'] == 'ethandrower+test11@gmail.com':
            #     print("updated email for test11 " + str(subscriber_info ))
            #     a = input('pause ')


        if len(subs) == 0:
            print("Create new sub")
            sub = models.Subscriber(trade_alerts=kwargs['alerts_on'], email=kwargs['subscriber_info']['email'], 
                                    alerts_email=kwargs['subscriber_info']['email'], phone=kwargs['subscriber_info']['phone'], 
                                    annual=kwargs['is_annual'], period_end=kwargs['period_end'])
            sub.save()

    except Exception as e:
        print("error in updae/create "+ str(e))
        #a = input('error in update' )

        #a = input('wait')

        


def get_stripe_subs(status=None, created=None):
    
    total_subs = []
    has_more = True
    starting_after = None

    while has_more:
        if starting_after:
            subs = stripe.Subscription.list(
                limit=100, status=status, starting_after=starting_after, created=created)
        else:
            subs = stripe.Subscription.list(limit=100, status=status, created=created)
        print("Subs found " + str(len(subs['data'])))
        for sub in subs['data']:
            total_subs.append(sub)
        time.sleep(1)

        if subs['has_more']:
            print("more subs indicated, setting last object id")
            starting_after = subs['data'][-1]['id']
            print("Starting after id set " + str(starting_after))
            time.sleep(1)
        else:
            print("no more subs, setting has_more to false")
            has_more = False
            time.sleep(1)

        print("Total Subs Length: " + str(len(total_subs)))
    total_subs.reverse()

    return total_subs



def update_daytrader(customer_id, email, is_daytrader):

    #is_daytrader = is_active_daytrader(customer_id, email)

    try:
        # if email == 'info@navigationtrading.com':
        #     print("info found")
        #     print(str(customer_id) + str(is_daytrader))
        #     a = input('stop')


        sub = models.Subscriber.objects.get(email=email)
        sub.is_daytrader = is_daytrader
        sub.save()
    except Exception as e:
        print("exception updating daytrader " + str(email))
        #a = input('stop')

        pass
        #a = input('error with update dt' + str(e))
        
def is_active_annual(customer_id, email):

    subs = stripe.Subscription.list(
                limit=100, customer=customer_id, status='active' )

    trials = stripe.Subscription.list(
                limit=100, customer=customer_id, status='trialing' )

    for trial in trials['data']:
        subs['data'].append(trial)

    
    for index, value in enumerate(subs['data']):

       
            #a = input('found')

        annual_plans =  ['prod_ICYJIrjTZiEOO7', 'prod_GFGbKUYYoClIKs', 'prod_G3JZ8n9YyLjANG', 'prod_G7hwJ00trrxdvf', 
                         'prod_G8GC29MqvBfVkJ', 'prod_GVhoi9E31YUh0D', 'prod_FvnwTx1qoqXbxz', 'prod_Fkv5GZkjWJmfO7',
                         'prod_FapHnkxjOV2CEe', 'prod_EemOQvOiUaGiJG', 'prod_DwYwBJBXAznt8S', 'prod_DN6nlP9Sq8By9L',
                         'prod_Byn9SglPWIA5nM', 'prod_BxJ4bXv6iJeZHg' ]

        if value['plan']['product'] in annual_plans: #prod_HhCuAniDH28J2r
            return True        
    
    return False


def is_pro(customer_id, email):
    subs = stripe.Subscription.list(
                limit=100, customer=customer_id, status='active' )

    trials = stripe.Subscription.list(
                limit=100, customer=customer_id, status='trialing' )

    for trial in trials['data']:
        subs['data'].append(trial)

    daytrading_stripe_ids = os.environ['DAYTRADING_COURSE_STRIPE_ID'].split('|')


    
    for index, value in enumerate(subs['data']):

        # if email == 'burnichfiber@gmail.com':
        #     print(str(subs['data']))
            #print("comparing daytrading id: " + daytrading_stripe_id + " with " + str(value['plan']['product']))

            #a = input('found')
            #  prod_HhCuAniDH28J2r  == daytrading id 
        if value['plan']['product'] not in daytrading_stripe_ids : #prod_HhCuAniDH28J2r
            return True        
    
    return False



def is_active_daytrader(customer_id, email):

    subs = stripe.Subscription.list(
                limit=100, customer=customer_id, status='active' )

    trials = stripe.Subscription.list(
                limit=100, customer=customer_id, status='trialing' )

    for trial in trials['data']:
        subs['data'].append(trial)

    daytrading_stripe_ids = os.environ['DAYTRADING_COURSE_STRIPE_ID'].split("|")

    for index, value in enumerate(subs['data']):

        if value['plan']['product'] in daytrading_stripe_ids:
            return True
    return False



def check_multi_subs(customer_id, email ):

    ## get subscriptions for customer id.
    subs = stripe.Subscription.list(
                limit=100, customer=customer_id, status='active' )

    trials = stripe.Subscription.list(
                limit=100, customer=customer_id, status='trialing' )

    for trial in trials['data']:
        subs['data'].append(trial)

    # if customer_id == 'cus_HEi7DOsjVoWNnT':
    #     print(str( subs['data']))

    #     a = input('customer found')
        
### new logic,  just delete the oldest subs
    daytrading_stripe_ids = os.environ['DAYTRADING_COURSE_STRIPE_ID'].split("|")

    for index, value in enumerate(subs['data']):

        if value['plan']['product'] in daytrading_stripe_ids:
            subs['data'].pop(index)
            print("removing daytrading obj from the list")

            print(str(subs['data']))
            #a = input('removed daytrader')

    if len(subs['data']) > 1:

        most_recent_sub = datetime(year=2000, month=1, day=1)
        subs_to_cancel = []




        for index, value in enumerate(subs['data']):
            created = datetime.fromtimestamp(value['created'])
            if created > most_recent_sub:
                print("found more recent datetime" + str(value))
                most_recent_sub = created
          


            else:
                if  value['plan']['interval'].lower() == 'month' and value['plan']['interval_count'] == 1:
                    interval  = 'monthly'
                    
                elif value['plan']['interval'].lower() == 'month' and value['plan']['interval_count'] == 6:
                    interval = '6month' 

                else:
                    interval = 'annual'
                subs_to_cancel.append({"id": value['id'], "interval": interval,  'sub_obj': value } )
        print("Subs to cancel: " + str(subs_to_cancel) )
    
        for sub in subs_to_cancel:
            pass
            #issue_partial_refund(customer_id, sub['sub_obj'])
            #stripe.Subscription.delete(sub['id'], prorate=False)
        #a = input('wait')
            #unenroll_thinkific(email, interval=sub['interval'])

        #customer_details = get_stripe_cust_details(customer_id)
            #send_upgrade_receipt(email)

    return True 
        ## now cancel all subs that aren't the most recent sub. 


def get_total_active_subs(active_subs, id):

    count = 0
    for sub in active_subs:
        if sub == id:
            count += 1
    return count


### old logic
    ## if 2 subscriptions then one is monthly one is somethign else.
    # if len(subs['data']) > 1:  ## one is monthly

    #     sub_objs = {"monthly": None, "6month": None, "annual": None}

    #     for index, value in enumerate(subs['data']):

    #         ##
    #         if  value['plan']['interval'].lower() == 'month' and value['plan']['interval_count'] == 1:
    #             sub_objs['monthly'] = value
                    
    #         elif value['plan']['interval'].lower() == 'month' and value['plan']['interval_count'] == 6:
    #             sub_objs['6month'] = value 

    #         else:
    #             sub_objs['annual'] = value 


        
    #     if sub_objs['monthly'] is not None  and sub_objs['6month'] is not None:

    #         remove = sub_objs['monthly']
    #         monthly = True 
    #          pass ## this is a month -> 6month upgrade
    #     elif sub_objs['monthly'] is not None  and sub_objs['annual'] is not None:
    #         remove = sub_objs['monthly']
    #         monthly = True 
    #     else:
    #         remove = sub_objs['6month']
    #         monthly = False 



    #     stripe.Subscription.delete(remove['id'], prorate=True)
    #     #a = input('wait')
    #     unenroll_thinkific(email, monthly=monthly)

    #     #customer_details = get_stripe_cust_details(customer_id)
    #     send_upgrade_receipt(email)
    
                ## send email.
    ## get today's date

    ## get when monthly sub next billing date is
    ## current_period_end": 1587131073,
    ##"current_period_start": 1584452673,
        ## cancel the monthly -immediately

        

        ## issue partial refund 
        ## send email receipt.




class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('--today', nargs='*', type=int)

    def handle(self, *args, **options):


        stripe.api_key = os.environ.get('STRIPE_SECRET')

        if options['today'][0] > 0:
            print("short parameter")
            today = datetime.today() - timedelta(hours=1)

            today = {'gte': int(datetime.timestamp(today))}

            print("timestamp =", today)
            
        else:
            today=None


        active_subs = get_stripe_subs(status='active', created=today)
        
        cancelled_subs = get_stripe_subs(status='canceled', created=today)
        #cancelled_subs = []
        trial_subs = get_stripe_subs(status='trialing', created=today)
        for sub in trial_subs:
            active_subs.append(sub)


        active_subs_ids = []
        for sub in active_subs:
            active_subs_ids.append(sub['customer'])
        
        active_subs.reverse()

        for sub in cancelled_subs:

            #active_subs_count =  get_total_active_subs(active_subs_ids, sub['customer'])

            #if sub['customer'] not in active_subs_ids or active_subs_count < 2: # if customer has an active sub dont update db  
             # if customer has an active sub dont update db  

            subscriber_info = get_stripe_cust_details(sub['customer'])
            if subscriber_info:
                try:

                        alerts_on = is_pro(subscriber_info['stripe_id'], subscriber_info['email'])


                        is_annual = is_active_annual(subscriber_info['stripe_id'], subscriber_info['email'])

                        #is_annual = sub['plan']['interval'].lower() != 'month'

                        update_or_create_subscriber(subscriber_info=subscriber_info, alerts_on=alerts_on, is_annual=is_annual, is_sixmonth=None, period_end=None)
                    
                        is_daytrader = is_active_daytrader(subscriber_info['stripe_id'], subscriber_info['email'])
                        update_daytrader(subscriber_info['stripe_id'], subscriber_info['email'], is_daytrader)
                        
                        print("updated subscriber " + str(subscriber_info['email'] ) )

                        log_string = "Alerts Status: {0}, Daytrader: {1}, Annual: {2} ".format(alerts_on, is_daytrader, is_annual)
                        write_log(status='SUCCESS', action='UPDATED CUSTOMER',
                              subscriber=subscriber_info['email'], comment=log_string)
                except Exception as e:
                    print("EXCEPTION update_or_Creatre subscriber "+ str(e))
                    write_log(status='FAILURE', action='UPDATED CUSTOMER',
                              subscriber=subscriber_info['email'], comment=str(e))

        for sub in active_subs:
            #print("Stripe Sub: " + str(sub['plan']['interval']) + str(sub['plan']['name']))
            subscriber_info = get_stripe_cust_details(sub['customer'])

            if subscriber_info:
        
                # if subscriber_info['email'] == 'ethandrower@gmail.com':
                #     print(str(sub))
                #     print( sub['current_period_end'])
                #     #a = input('pause ')

                period_end = datetime.fromtimestamp(sub['current_period_end'])
                    
                check_multi_subs(sub['customer'], subscriber_info['email'])
                alerts_on = is_pro(subscriber_info['stripe_id'], subscriber_info['email'])

                if subscriber_info['email'] == 'burnichfiber@gmail.com':
                    print("burnich test alerts_on val " + str(alerts_on))
                    #a = input('wait')


                is_annual = is_active_annual(subscriber_info['stripe_id'], subscriber_info['email'])
                #is_annual = sub['plan']['interval'].lower() != 'month'
                months = sub['plan']['interval_count'] 
                
                #daytrading_stripe_id = os.environ['DAYTRADING_COURSE_STRIPE_ID']
                

                #is_daytrader= is_active_daytrader(subscriber_info['stripe_id'], subscriber_info['email'])

                #x = 10 if a > b else 11
                #is_daytrader = True if sub['plan']['product'] == daytrading_stripe_id else None
                


                if months == 6:
                    is_6month = True 
                else:
                    is_6month = None 
 

                try:
                    #update_or_create_subscriber(subscriber_info=subscriber_info, alerts_on=False, is_annual=None, is_sixmonth=None, period_end=None, is_daytrader=None)

                    update_or_create_subscriber(subscriber_info=subscriber_info, alerts_on=alerts_on, 
                                                is_annual=is_annual, is_sixmonth=is_6month,  period_end=period_end)

                    is_daytrader = is_active_daytrader(subscriber_info['stripe_id'], subscriber_info['email'])
                    update_daytrader(subscriber_info['stripe_id'], subscriber_info['email'], is_daytrader)
                    
                    log_string = "Alerts Status: {0}, Daytrader: {1}, Is Annual: {2}".format(alerts_on, is_daytrader, is_annual)

                  
                    write_log(status='SUCCESS', action='UPDATED CUSTOMER',
                              subscriber=subscriber_info['email'], comment=log_string)
                except Exception as e:
                    print("EXCEPTION Updating or creating subscriber:  " + str(e))
                    
                    #a = input('exception stop')

                    write_log(status='FAILURE', action='UPDATED CUSTOMER',
                              subscriber=subscriber_info['email'])
            else:
                print("No subscriber info: " )
                #a = input('no sub')

                #if is_annual is False:
                #    period_end = None


                # if subscriber_info['email'] == 'steve@clearpointvideo.com':

                #     print(sub['plan']['interval'])

                #     a = input('steve sub found ')

                

from __future__ import print_function
import clicksend_client
from clicksend_client.rest import ApiException
from django.core.management.base import BaseCommand, CommandError


from api import models

import api.clicksend_funcs as clicksend_funcs
from api.utilities.discourse_utils import post_to_discourse
from api.emailer import send_alert
import traceback

### pull all subscribers active from our db where clicksend_sync = false





## 

## add each contact to the list.


class Command(BaseCommand):
    help = 'Executes hourly pull data'

    def add_arguments(self, parser):
        parser.add_argument('user_id', nargs='*', type=int)

    def handle(self, *args, **options):


        # hit API for trades.


        
        # check if trade has been posted.
        subs = models.Subscriber.objects.filter(trade_alerts=True,)
        #subs = models.Subscriber.objects.filter(id=1405)        
       # print("WARNING DEBUG Subs Enabled") 

        triggers = models.NotifyTrigger.objects.filter(status='NEW')

        if len(triggers) > 0:
            print("trades found to start sending notifications on!")

            for trig in triggers:
                trig.status = 'IN PROGRESS'
                trig.save()

                trade = models.Trade.objects.get(id=trig.trade.id)

                for sub in subs:

                    nr = models.NotifyResult(trade=trade, subscriber=sub)
                    nr.save()

                ### Discourse psot reference.

                try:
                    res0 =  post_to_discourse(trade)
                except Exception as e:
                    print("Exception sending  Discourse alerts: " + str(e))
                    send_alert("Discourse alert exception " + str(e)+ str(traceback.format_exc()))

                try:
                
                    res1 = clicksend_funcs.send_email_alerts(subs, trade)
                except Exception as e:
                    print("Exception sending email alerts: " + str(e))
                    send_alert("Email alert exception " + str(e)+ str(traceback.format_exc()))
                try:

                    res2 = clicksend_funcs.send_sms_alerts(subs, trade)

                except Exception as e:
                    print("Exception sending sms alerts: " + str(e))

                    send_alert("SMS alert exception " + str(e)+ str(traceback.format_exc()))

                print("completed both clicksend batch functions")
        else:
            print("no trades found")







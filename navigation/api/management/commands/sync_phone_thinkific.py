
## sync phone numbers from thinkific





# {
#     "items": [
#         {
#             "id": 12373775,
#             "created_at": "2019-10-28T07:45:33.451Z",
#             "first_name": "Ethan",
#             "last_name": "Drower",
#             "full_name": "Ethan Drower",
#             "company": null,
#             "email": "ethandrower@gmail.com",
#             "roles": [],
#             "avatar_url": null,
#             "bio": null,
#             "headline": null,
#             "affiliate_code": null,
#             "external_source": null,
#             "affiliate_commission": null,
#             "affiliate_commission_type": "%",
#             "affiliate_payout_email": null,
#             "administered_course_ids": null,
#             "custom_profile_fields": [
#                 {
#                     "id": null,
#                     "value": null,
#                     "label": "phone",
#                     "custom_profile_field_definition_id": 17561
#                 }
#             ]
#         }
#     ],



import requests
from api.models import Subscriber

from api.logger import write_log

from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError

import api.emailer as emailer
from django.template.loader import render_to_string
import time
import os 


def send_email_reminder(email):
    

    text = render_to_string('phone-reminder.html',)


    res = emailer.send_phone_reminder(email, text)


    # get template and render it.

    # send email reminder via requests.



def process_user_phone(thinkific_json_obj):

    if len(thinkific_json_obj['items']) == 1:

        phone = thinkific_json_obj['items'][0]['custom_profile_fields'][0]['value']
        email = thinkific_json_obj['items'][0]['email']

        try:

            subscriber = Subscriber.objects.get(email=email)

            
            subscriber.phone = phone.replace(".0","").strip()

 
            subscriber.save()


            if phone is None and subscriber.trade_alerts is True and subscriber.sent_phone_reminder is not True:
                #send_email_reminder(email)
                pass
            print("updated phone number in database")

            return True
        except Exception as e:
            print("No Subscriber found to update: " + str(e))
            print(str(thinkific_json_obj))

            return False
    else:
        print("No user found in Thinkific")


        # paying subscriber hasn't updated phone.  send one time email.



    #write_log here




def send_api_request(email):

    url = "https://api.thinkific.com/api/public/v1/users"

    query_string =  {"query[email]": email}

    api_key = os.getenv('THINKIFIC_API_KEY', None)
    print('got api key from env ' + api_key)


    headers = {
        'X-Auth-API-Key':  api_key,
        'X-AUTH-Subdomain': 'navigationtrading'


    }
    res = requests.get(url, headers=headers, params=query_string)

    if res.status_code < 299:
        print('success in get users')

        print(str(res.json()))
        return res.json()
    else:
        print("error" + str(res) + str(res.text))
        return False 


    






class Command(BaseCommand):
    help = 'Updates Phone Numbers in DB with Thinkific Profile Values'

    def add_arguments(self, parser):
        parser.add_argument('recent_subs', nargs=1, type=int)
        pass
    def handle(self, *args, **options):

        # if recent subs == 1 

#        send_email_reminder("ethandrower@gmail.com")
        
        #return True



        if options['recent_subs'] == 1:

            today = datetime.today()
            yesterday = today - timedelta(days=1)

            subscribers = Subscriber.objects.filter(trade_alerts=True, last_updated__gte=yesterday)

        else:
            # we are on slow interval
            subscribers = Subscriber.objects.filter(trade_alerts=True)


        for sub in subscribers:
            print("Updating phone for subscriber: " + str(sub.email))
            thinkific_response = send_api_request(sub.email)
            time.sleep(.5)
            if thinkific_response is False:
                # response was successful process phone
                print("Throttled, wait and retry...")
                time.sleep(5)
                thinkific_response = send_api_request(sub.email)
                if thinkific_response == False:
                    print("error with phone sync service")
                    emailer.send_alert("Error with Phone Sync Service")
 
            
            print("thinkific_response success: " +str(thinkific_response))

                
            result = process_user_phone(thinkific_response)

                


from __future__ import print_function
import clicksend_client
from clicksend_client.rest import ApiException
from clicksend_client import  SmsMessage
from clicksend_client import EmailRecipient
from clicksend_client import EmailFrom
import os 
import json
import ast
import api.models as models
from datetime import datetime, timedelta
from django.template.loader import render_to_string

import time
import traceback 

from api.emailer import send_alert, send_trade_alert_mailgun
from api.utils import get_dte, generate_dtes, chop_futures_underlying, chop_w_underlying
import logging 


def send_email(email, template_name):

    
    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("Sending email alerts" )
    api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))


    email_msg = render_to_string(template_name )
    email_sub = 'Receipt '

    email_recipient=EmailRecipient(email=email,)
        
    email_from=EmailFrom(email_address_id=613,name='NavigationALERT')

    email = clicksend_client.Email(to=[email_recipient],
                              _from=email_from,
                              subject=email_sub,
                              body=email_msg,
                              ) 

    api_response = api_instance.email_send_post(email)
    print(api_response)



def send_upgrade_receipt(email):
    
    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("Sending email alerts" )
    api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))


    email_msg = render_to_string('upgrade-receipt.html' )
    email_sub = 'Receipt '

    email_recipient=EmailRecipient(email=email,)
        
    email_from=EmailFrom(email_address_id=613,name='NavigationALERT')

    email = clicksend_client.Email(to=[email_recipient],
                              _from=email_from,
                              subject=email_sub,
                              body=email_msg,
                              ) 

    api_response = api_instance.email_send_post(email)
    print(api_response)

def send_discount_receipt(email):
    
    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("Sending email alerts" )
    api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))


    email_msg = render_to_string('discount-receipt.html' )
    email_sub = 'Receipt from Navigation Trading'

    email_recipient=EmailRecipient(email=email,)
        
    email_from=EmailFrom(email_address_id=613,name='NavigationALERT')

    email = clicksend_client.Email(to=[email_recipient],
                              _from=email_from,
                              subject=email_sub,
                              body=email_msg,
                              ) 

    api_response = api_instance.email_send_post(email)
    print(api_response)


def send_cancellation_confirmation(email):
    
    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("Sending email alerts" )
    api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))


    email_msg = render_to_string('cancellation-reminder.html' )
    email_sub = 'Navigation Trading Cancellation Confirmation'

    email_recipient=EmailRecipient(email=email,)
        
    email_from=EmailFrom(email_address_id=613,name='NavigationALERT')

    email = clicksend_client.Email(to=[email_recipient],
                              _from=email_from,
                              subject=email_sub,
                              body=email_msg,
                              ) 

    api_response = api_instance.email_send_post(email)
    print(api_response)


def gen_email(trade, trade_legs):


    if trade.new_dte is not None or trade.prev_dte is not None:
        dte_obj = {'new_dte': trade.new_dte, 'prev_dte': trade.prev_dte } 
    else:

        dte_obj = generate_dtes(trade,trade_legs)
  
    email =  render_to_string('email-trade-alert.html', {'trade': trade, 'legs': trade_legs, 'new_dte': dte_obj['new_dte'], 'prev_dte': dte_obj['prev_dte']})
    print("email built " + str(email))

    return email 
# create an instance of the API class

# send list of subscribers their sms alerts in 1k chunks.
def send_sms_alerts(subscriber_list, trade):

    logger = logging.getLogger('django')
    logger.info('inside sending sms alerts - logger')

    print("inside sending sms_alerts")

    print("subscriber list " + str(len(subscriber_list))  + str(subscriber_list))

    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("before api instance declaration")

    api_instance = clicksend_client.SMSApi(clicksend_client.ApiClient(configuration))
    print("after api_instance") 
    
    message_queue = []
    sending_batches = []
    trade.underlying = chop_futures_underlying(trade.underlying)
    trade.underlying = chop_w_underlying(trade.underlying)
 

    #subscriber_list = [{"phone":"13126007326", "email": "ethandrower@gmail.com",},
     #                   {"phone": "8167216555",  "email": "info@navigationtrading.com" }]

    for sub in subscriber_list:
        print("in subscriber list loop")

        trade_msg= """NavigationALERT: \n Trade Type: {0} \n   Strategy: {1} \n  Symbol: {2} \n \n ToS Fill: \n {3}""" \
        .format(trade.trade_type,   trade.initial_strategy,  trade.underlying, trade.tos_string )
        
        if trade.tos_string2:
            trade_msg += """ \n {0} """.format(trade.tos_string2)


        if sub.id in [399, 94, 106 ]:
            print("canada number found, switching from" )
            sms_from = "+16473600023"
        else:
            sms_from = "+18444132521"
            #sms_from = "+18162536911" this was old dedicated number, trying toll free 


            


        sms_message = SmsMessage(source="php", _from=sms_from,  body=trade_msg,to=sub.phone )
        #sms_message = SmsMessage(source="php", body=trade_msg,to=sub.phone, schedule=1436874701)


        message_queue.append(sms_message)
    print("after queuing messages")
    print(str(message_queue))

    while len(message_queue) >= 999:
        batch = message_queue[0:999]
        sending_batches.append(batch)
        message_queue = message_queue[999:]

    sending_batches.append(message_queue)

    print("sending batches: " + str(sending_batches))

    for batch in sending_batches:

        sms_messages = clicksend_client.SmsMessageCollection(messages=batch)

        try:
            # Send sms message(s)
            api_response = api_instance.sms_send_post(sms_messages)
            print("SMS response: " + str( api_response))
            # create notify result if doesn't exist.
            print(type(api_response))
            api_response = ast.literal_eval(api_response)

            print(api_response['data'])
            print(api_response['data']['messages'])

            for message in api_response['data']['messages']:
                if 'to' in message:
                    phone = message['to'].replace('+1', '')
                    print("phone match is: " + str(phone))
                    message_id = message['message_id']
                    print("MEssage id: " + str(message_id))
                    
                    try:
                        notify_result = models.NotifyResult.objects.filter(subscriber__phone=phone).order_by('timestamp')[0]


                        print('got NR subscriber associated')
                        notify_result.sms_message_id = message_id
                        notify_result.sms_error = message['status']
                        
                        if message['status'] == 'INVALID_RECIPIENT':
                            print("Bad number recipient, notify admin")
                            send_alert(str(api_response))
                            notify_result.sms_result = False
                            notify_result.sms_error = 'INVALID_RECIPIENT'



                        notify_result.save()

                    except Exception as e:
                        print("exception getting notify result objs for phone subscriber " + str(e))


        except ApiException as e:
            print("Exception when calling SMSApi->sms_send_post:")
            # should notify us somehow directly of an issue.




def send_email_alerts(subscriber_list, trade):


    logger = logging.getLogger('django')
    logger.info('inside sending email alerts - logger')

    try:

        configuration = clicksend_client.Configuration()
        configuration.username = os.environ.get('CLICKSEND_USERNAME')
        configuration.password = os.environ.get('CLICKSEND_KEY')
        print("Sending email alerts" )
        api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))

        trade.underlying = chop_futures_underlying(trade.underlying)
        
        trade.underlying = chop_w_underlying(trade.underlying)

        trade_legs = models.TradeLegs.objects.filter(trade=trade, alerts_enabled=True)

        legs = "Trade LEgs here"

        email_msg = gen_email(trade, trade_legs)
        email_sub = str(trade.trade_type + " - " + trade.underlying)

  #      email_msg = """Trade: {0} \n
       #                 IV: {1} \n
       #                 Legs{3} \n
       #                 Comments: {2} 
        #                       """.format(trade.trade_type, str(trade.iv_percentile), legs, trade.comments)
                               


        #subscriber_list = [{"phone":"13126007326", "email": "ethandrower@gmail.com",},
         #               {"phone": "8167216555",  "email": "info@navigationtrading.com" }]
        for sub in subscriber_list:

            #sms_message = SmsMessage(source="php", body=str(trade),to=sub.phone, )

            #email_recipient=EmailRecipient(email=sub["email"],)
            email_recipient=EmailRecipient(email=sub.alerts_email,)
            #email_recipient=EmailRecipient(email=sub.email,)
            
            email_from=EmailFrom(email_address_id=613,name='NavigationALERT')





            email = clicksend_client.Email(to=[email_recipient],
                                  _from=email_from,
                                  subject=email_sub,
                                  body=email_msg,
                                  ) 

            try:
                # Send sms message(s)
                api_response = api_instance.email_send_post(email)
               # logger.info('Email scheduled for user: ' + str(sub.email))
                logger.info('API Response from CLicksend: ' + str(api_response))
                
                print(api_response)
                # create notify result if doesn't exist.
                print(type(api_response))
                api_response = ast.literal_eval(api_response)

                print(api_response['data'])
                

                #### MAILGUN TEST HERE #########
                #send_trade_alert_mailgun(email_msg,sub["email"] )
                

                email = api_response['data']['to'][0]['email']

                print("email found " + str(email))
                message_id = api_response['data']['message_id']
                try:

                    notify_result = models.NotifyResult.objects.get(subscriber__email=email, email_message_id=None)

                    print('got NR subscriber associated')
                    notify_result.email_message_id = message_id
                    notify_result.save()

                except Exception as e:
                    print("Exception finding single notify result ") 


            except ApiException as e:
                print("Exception when calling Email API > " + str(e))

    except Exception as e:
        print("caught exception inside send email alerts func : " + str(e))
        send_alert('Exception sending emails ' + str(e) + traceback.format_exc())


def get_email_history():

    today = datetime.today() - timedelta(days=100)
        
    tstamp = time.mktime(today.timetuple())

    current_page = 1

    
    last_page = 100
    while current_page < last_page +1:


        # get page from clicksend 

        

        api_instance = clicksend_client.TransactionalEmailApi(clicksend_client.ApiClient(configuration))
        date_from = tstamp # int | Start date (optional)
        #date_to = 1546923229 # int | End date (optional)
        page = 1 # int | Page number (optional) (default to 1)
        limit = 10 # int | Number of records per page (optional) (default to 10)

        try:
            # Get all transactional email history
            api_response = api_instance.email_history_get(date_from=date_from, page=page, limit=1000)
            print(api_response)
            api_response = ast.literal_eval(api_response)

            ## process api_response.

            for email in api_response['data']['data']:

                print(email['message_id'])

                try:

                    email_log = models.NotifyResult.objects.get(email_message_id=email['message_id'])
                except Exception as e:

                    print("no email found for this id")
                    continue 


                if email['status'] == 'Delivered':
                    email_log.email_result = True

                elif email['status'] =='Scheduled':
                    pass
                    # do nothing because it hasn't been sent.

                else:
                    # delivered and not schedule so definitely an error


                    email_log.email_result = False
                    email_log.email_error = email['status_text']

                email_log.save()


        except ApiException as e:
            print("Exception ")

        current_page += 1
        last_page = api_response['data']['last_page']
        print("last page is: " + str(last_page))
        print("current page is " + str(current_page))

        time.sleep(5)


def get_sms_message_history():
    

    configuration = clicksend_client.Configuration()
    configuration.username = os.environ.get('CLICKSEND_USERNAME')
    configuration.password = os.environ.get('CLICKSEND_KEY')
    print("CLicksend creds")

    print(configuration.username + ' ' + configuration.password)


    sms_logs = models.NotifyResult.objects.all().exclude(sms_result=True).exclude(sms_result=False)

    for sms_log in sms_logs:

        api_instance = clicksend_client.SMSApi(clicksend_client.ApiClient(configuration))


        try:
            # Get a Specific Delivery Receipt
            print(sms_log.sms_message_id)

         #   api_response = api_instance.sms_receipts_by_message_id_get(sms_log.sms_message_id)

            #api_response = api_instance.sms_receipts_by_message_id_get('E36F898A-9D2E-46D6-B5BD-637D9B77F76E')
            api_response = api_instance.sms_receipts_get(page=1, limit=100)          
            print(api_response)
            api_response = ast.literal_eval(api_response)
        except ApiException as e:
            print("Exception when calling SMSA" + str(e))

        time.sleep(1)

function initSearch() {
  $('.js-search').on('input paste', function() {
    var value = $(this).val().toLowerCase().trim();
    var $table = $('.js-trades-table');

    $table.find('tr').each(function(index) {
      var $tr = $(this);
      if (!index) return;
      $(this).find('td').each(function() {
        var text = $(this).text().toLowerCase().trim();
        var not_found = (text.indexOf(value) == -1);
        $tr.toggle(!not_found);
        return not_found;
      });
    });
  });
};


$(function() {
  initSearch();
});

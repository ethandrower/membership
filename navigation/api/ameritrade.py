import json
import os
import requests
# ameritrade api creds.
import api.models as models
from datetime import datetime, timedelta


from api.emailer import send_new_auth 

def send_full_auth_email():
    #this wil lsend to user to auth via ameritrade credentials

    current_env = os.environ.get('ENVIRONMENT')

    if current_env == 'DEV':
        #send_new_auth('')
        #pass
       
        send_new_auth('https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=https%3A%2F%2Fadmin.navigationtrading.com%2Fnavigation&client_id=JVTWKIENB6PQZMC6GKXPY7ALEKWRTJJH%40AMER.OAUTHAP')
        #send_new_auth('https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=https%3A%2F%2Fnavigation.drowerconsultants.com%2Fnavigation&client_id=JVTWKIENB6PQZMC6GKXPY7ALEKWRTJJH%40AMER.OAUTHAP')


    elif current_env == 'PROD':
        pass
#send_new_auth('https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=https%3A%2F%2Fnavigation.drowerconsultants.com%2Fnavigation&client_id=JVTWKIENB6PQZMC6GKXPY7ALEKWRTJJH%40AMER.OAUTHAP')
    # send email


def refresh_token(refresh_code):

    # make request with refresh code

    current_env = os.environ.get('ENVIRONMENT')
    token_obj = models.AmeritradeAuth.objects.get(environment=current_env)
    refresh_token = token_obj.refresh_token
    redirect_uri = token_obj.redirect_uri
    

    headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
    data = { 'grant_type': 'refresh_token', 'refresh_token': refresh_token, 'client_id': 'JVTWKIENB6PQZMC6GKXPY7ALEKWRTJJH', 'redirect_uri': redirect_uri }
    auth_reply = requests.post('https://api.tdameritrade.com/v1/oauth2/token', headers=headers, data=data)

    print(" refresh_token status code = " + str(auth_reply.status_code ) + str(auth_reply.text))
 
 
    if auth_reply.status_code == 401 or auth_reply.status_code == 400:
        # we neeed to reauth, send email with link
        print("sending auth email")
        send_full_auth_email()


    else:
        print("different auth_reply other than 401 ") 

        json_data = json.loads(auth_reply.text)
        new_token = json_data['access_token']

        current_env = os.environ.get('ENVIRONMENT')
        token_obj = models.AmeritradeAuth.objects.get(environment=current_env)
        token_obj.access_token = new_token
        token_obj.save()
        print("refreshed token method completed")
        return True 




def get_recent_orders():

    # get token from DB.
    current_env = os.environ.get('ENVIRONMENT')
    account_number = os.environ.get('AMERITRADE')

    token_obj = models.AmeritradeAuth.objects.get(environment=current_env)
    headers = { 'Authorization': 'Bearer ' + str(token_obj.access_token) }

    # 686056422  = ethan live account
    # 488220184  = navigagtion account
    url = 'https://api.tdameritrade.com/v1/accounts/' + str(account_number) + '/transactions/'
    url = 'https://api.tdameritrade.com/v1/accounts/' + str(account_number) + '/orders/' #+ '2302753236'
   

    #url = 'https://api.tdameritrade.com/v1/' +'orders/'
    #url = 'https://api.tdameritrade.com/v1/accounts/' + account_number 

    today = (datetime.today()- timedelta(days=2)).strftime('%Y-%m-%d')

    print("today " + today) 
    #params = {'startDate':  today }
    #params = { 'fields': 'positions,orders'}
    params = {}
    account_reply = requests.get(url, headers=headers, params=params)

    if account_reply.status_code != 201 and account_reply.status_code != 200:

        print('status code returned ' + str(account_reply.status_code ))
        print("issue with account, refreshing token " + str(account_reply.text))
        refresh_status = refresh_token(token_obj.refresh_token)
        if refresh_status:
            print("refreshed: ")
            token_obj = models.AmeritradeAuth.objects.get(environment=current_env)
            headers = { 'Authorization': 'Bearer ' + str(token_obj.access_token) }
            account_reply = requests.get(url, headers=headers, params=params)
            json_data = json.loads(account_reply.text)
            
    else:
        #was 201 so parse it.
        print("200 response so parse. ")
        json_data = json.loads(account_reply.text)
        
    with open ('account-dump2.txt', 'w') as f:
        f.write(account_reply.text)

    print("returing json data " + str(json_data))
    return json_data


    # parse orders now and see if they are already in database
    # if not, then we need to trigger an ew trade, create new legs.




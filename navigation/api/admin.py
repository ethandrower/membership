from django.contrib import admin

# Register your models here.


from api.models import Trade, TradeLegs, Subscriber



class TradeAdmin(admin.ModelAdmin):
	model = Trade 

	list_display = ('id', 'trade_time', 'initial_strategy', 'underlying', 'tos_string' )


class SubscriberAdmin(admin.ModelAdmin):

	model = Subscriber
	list_display = ('email', 'phone', 'trade_alerts', 'annual')
	search_fields = ('email', 'phone' )


admin.site.register(Trade, TradeAdmin)
admin.site.register(TradeLegs)
admin.site.register(Subscriber, SubscriberAdmin)


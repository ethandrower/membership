from django.db import models

# Create your models here.


class AmeritradeAuth(models.Model):

	environment = models.CharField(max_length=100)
	access_token = models.CharField(max_length=10000)
	refresh_token = models.CharField(max_length=10000)
	redirect_uri = models.CharField(max_length=10000)


class SSOAuth(models.Model):

	nonce =models.CharField(max_length=250)
	email = models.CharField(max_length=250)

	sig = models.CharField(max_length=250)
	external_id = models.IntegerField(null=True, blank=True)


class Trade(models.Model):
	trade_time = models.DateTimeField(auto_now_add=True, editable=True)
	trade_type = models.CharField(max_length=250)
	mother_trade = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
	screen_shot = models.ImageField(null=True, blank=True)
	screen_shot2 = models.ImageField(null=True, blank=True)
	screen_shot3 = models.ImageField(null=True, blank=True)

	#legs
	#initial_strategy =
	td_id = models.CharField(max_length=100, null=True, blank=True)
	buy_sell = models.CharField(max_length=10, null=True, blank=True)
	spread_volume = models.IntegerField(null=True, blank=True)
	spread_fill = models.DecimalField( decimal_places=2, max_digits=15 )
	comments = models.CharField(max_length=2000, null=True, default=None)
	iv_percentile = models.IntegerField(null=True, blank=True)
	
	initial_strategy = models.CharField(max_length=150, null=True, blank=True)
	status = models.CharField(max_length=50, null=True, blank=True)
	tos_string = models.CharField(max_length=1000, null=True, blank=True)
	tos_string2 = models.CharField(max_length=1000, null=True, blank=True)
	underlying = models.CharField(max_length=20, null=True, blank=True)
	send_alerts = models.BooleanField(default=None, blank=True, null=True)

	profit = models.DecimalField(decimal_places=2, max_digits=15, null=True, blank=True)
	is_api_trade = models.BooleanField(default=None, blank=True, null=True)
	new_dte = models.IntegerField(null=True, blank=True, default=None)
	prev_dte = models.IntegerField(null=True, blank=True, default=None)

class TradeLegs(models.Model):

	trade = models.ForeignKey(Trade, on_delete=models.CASCADE)
	contract =  models.CharField(max_length=100) # SPY Sep 19 Put 
	contract_readable = models.CharField(max_length=250, null=True, blank=True)
	
	strike = models.DecimalField( decimal_places=2, max_digits=15, null=True, blank=True)
	put_call = models.CharField(max_length=6, null=True, blank=True)
	volume = models.IntegerField()
	price = models.DecimalField( decimal_places=2, max_digits=15, null=True, blank=True)
	buy_sell = models.CharField(max_length=10, null=True, blank=True)
	status = models.CharField(max_length=50, null=True, blank=True)

	expiration = models.DateTimeField(null=True, blank=True)
	
	alerts_enabled = models.BooleanField(null=True, default=True)
	

class Reminder(models.Model):

	created_on = models.DateTimeField(auto_now_add=True)
	email = models.CharField(max_length=100, null=False, blank=False)
	complete = models.BooleanField(default=False)
	


class Subscriber(models.Model):
	trade_alerts = models.BooleanField(default=None, blank=False, null=True)
	annual = models.BooleanField(default=None, blank=False, null=True)
	sixmonth = models.BooleanField(default=None, blank=False, null=True)
	lifetime = models.BooleanField(default=None, blank=False, null=True)
	
	email = models.CharField(max_length=250, blank=False, unique=True)
	alerts_email = models.CharField(max_length=250, blank=True)
	last_updated = models.DateTimeField(auto_now_add=True)
	phone = models.CharField(max_length=30, null=True, blank=True)
	clicksend_sync = models.BooleanField(default=None, blank=True, null=True)
	period_end = models.DateTimeField(default=None, blank=True, null=True)
	stripe_id = models.CharField(max_length=50, null=True, blank=True)
	sent_phone_reminder = models.BooleanField(default=None, blank=True, null=True)
	cancel_pending = models.BooleanField(default=None, blank=True, null=True)
	clickfunnel_legacy = models.BooleanField(default=False, blank=True, null=True)
	is_daytrader = models.BooleanField(default=False, blank=True, null=True)
	discord_id = models.CharField(max_length=50, null=True, blank=True)

	

	
class Log(models.Model):

	ACTION_CHOICES = (
		('SEND ALERT', 'Send Alert'),
		('QUERY STRIPE', 'Query Stripe'),
		('UPDATED SUBSCRIBER', 'Updated Subscriber'),
		('AMERITRADE SYNC', 'Ameritrade Sync')



	)

	STATUS_CHOICES = (

		('SUCCESS', 'Success'),
		('FAILURE', 'Failure'),
		('INFO', 'Info')

	)
	status = models.CharField(max_length=100,
			choices=STATUS_CHOICES, null=True, blank=True)

	time = models.DateTimeField(auto_now_add=True)
	
	action = models.CharField(max_length=100,
			choices=ACTION_CHOICES, null=True, blank=True)

	trade = models.ForeignKey(Trade, on_delete=models.CASCADE, blank=True, null=True)
	subscriber = models.ForeignKey(Subscriber, on_delete=models.CASCADE, blank=True, null=True)
	comment = models.CharField(max_length=1000, null=True, blank=True)





class NotifyTrigger(models.Model):

	STATUS_CHOICES = (
		('NEW', 'New'),
		('IN PROGRESS', 'In Progress'),
		('COMPLETE', 'Complete'),
		('COMPLETED WITH ERRORS', 'Completed With Errors'),
		('NOT SCHEDULED', 'Not Scheduled')
		)

	timestamp = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=100, 
        	choices= STATUS_CHOICES, null=True, blank=True)

	discord_status = models.BooleanField(default=None, blank=True, null=True)
	trade = models.ForeignKey(Trade, on_delete=models.CASCADE, related_name='notify_triggers')

	



class NotifyResult(models.Model):

	timestamp =  models.DateTimeField(auto_now_add=True)
	subscriber = models.ForeignKey(Subscriber, on_delete=models.CASCADE)
	trade = models.ForeignKey(Trade, on_delete=models.CASCADE)
	sms_result = models.BooleanField(blank=True, null=True)
	sms_message_id= models.CharField(max_length=250, null=True, blank=True)

	email_result = models.BooleanField(blank=True, null=True)
	email_message_id = models.CharField(max_length=250, null=True, blank=True)

	sms_error = models.CharField(max_length=500, default=None, blank=True, null=True)
	email_error = models.CharField(max_length=500, default=None, blank=True, null=True)

# can onboard intake forms based on their old CERs

# we can expose our frontend to them.

# what's their current certifications + timelines 
# lit review 75% man hours coincides with post market surveillance
# fixed base price for them, scales based on results. 



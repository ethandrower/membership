NavigationTrading ReadME

Architecture overview can be found here 

https://docs.google.com/document/d/14C5ffc-o1sT2pMxMpgNuyPn9pA0jC7NUXgfbzfjlxuM/edit?usp=sharing


Servers:
---------------

Host nav_staging  (development non-production)
    User root
    Hostname 165.22.176.210


Host production-api  ( main production server)
    User root
    Hostname 159.203.185.139
    IdentityFile ~/.ssh/id_rsa
    Port 22

Databases
---------------------
I believe we are using a managed Digital Ocean database for both production and development databases. 





Installation and Running Django Project
--------------------------------------------------

- Install all pip requirements.txt
- select .dev-env or .prod-env file
- python3 manage.py runserver


Deployment
------------------------------------

We have two servers, both are deployed simply via git (not ideal)

Using linux screen package
https://linuxize.com/post/how-to-use-linux-screen/

- connect to linux screen session

    root@dev-api:~# screen -ls
    There is a screen on:
	1698.ethan	(06/16/2020 10:27:20 AM)	(Detached)
    root@dev-api:~# screen -r ethan

- if no screen session create one 

    root@dev-api:~# screen -S ethan


Now launch Django server in one of the screens

    python3 manage.py runserver


Open second screen (ctrl a then c )

    cd discord_bot
    python3 discord_bot.py



Updating with new code 
-------------------------

Nothing fancy here, simply

    git pull origin [ your branch you want to run ]

Then restart discord_bot if you modified that code with the recent pull.

--- 



Crontab Documentation
---------------------------
The following cronjobs run:



* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 uptime_checker/check_api.py


This simply tries to HTTP GET some url at the django API, and if fails multiple times it sends email to me to notify of downtime.

---


* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py check_for_trades >> /var/log/check_for_trades.log

This checks our database to see if any new Trades (and send alerts if they are new)

---

* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py sync_ameritrade >> /var/log/sync_ameritrade.log

This is the command that syncs with TD Ameritrade API to sync latest trades.

--- 



* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py update_cache 'OPEN' >> /var/log/update_cache.log

This is a caching process, it builds an HTML file that gets sent to the Thinkific frontend whenever a user logs into their account and clicks "View Trades" view.

---


* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py update_cache 'CLOSED' >> /var/log/update_cache.log

Same thing as above, but for CLOSED trades

--- 


* * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py sync_subscribers --today 1

This queries Stripe for all subscriptions and updates our database to reflect new subscribers AND cancelled/expired subscribers.  Is like an ETL  form Stripe -> Django

--- 

5 * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py sync_subscribers --today 0

Same as above but faster interval (and only looks for subscriptions created today)

---


1 * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py reminders  >> /var/log/reminders.log 

This looks for subscribers about to cancel and reminds them to end their subscription.

--- 

4 * * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py sync_phone_thinkific 1 

This queries Thinkific API to see if a user has updated their phone number within thinkific.  If yes it syncs with Django

--- 

2 5 * * * source /root/membership/3env/bin/activate && source /root/membership/.dev-env && cd /root/membership/navigation/ && python3 manage.py sync_phone_thinkific 0 
                                              
Same as above 

--- 

--- 

